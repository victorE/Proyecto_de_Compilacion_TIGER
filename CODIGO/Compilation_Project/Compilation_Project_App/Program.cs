﻿using System;
using Antlr.Runtime;
using Compilation_Project.Parsing;
using Compilation_Project.AST;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using Compilation_Project.Semantic;
using System.Reflection;
using System.Reflection.Emit;

namespace Compilation_Project_App
{
    class Program
    {

        static int Main(string[] args)
        {
            Console.WriteLine("Compilation Project version 3.0");
            Console.WriteLine(@"Copyright (C) 2016-2017 Victor Ernesto Puentes Perez C-411");

            //string path = "C:\\Users\\victor\\Documents\\Visual Studio 2015\\Projects\\Compilation_Project\\Compilation_Project_App\\bin\\Debug\\a.tig";
            //return Compile(path);


            if (args.Length < 1 || !File.Exists(args[0]) || Path.GetExtension(args[0]) != ".tig")
            {
                Console.WriteLine("({0},{1}) - {2}", 0, 0,
                    args.Length < 1 ? "A file must be especified" : !File.Exists(args[0]) ? string.Format("File {0} cannot found", args[0])
                    : "The extension of the file must be .tig");
                return 1;
            }
            return Compile(args[0]);
        }

        public static int Compile(string path)
        {
            try
            {
                ANTLRFileStream input = new ANTLRFileStream(path);

            // Syntactic Analysis
            GrammarLexer lexer = new GrammarLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            GrammarParser parser = new GrammarParser(tokens);
            parser.TreeAdaptor = new Adaptor();

            // Semantic Analysis
            var tree = parser.program().Tree as ProgramNode;
            var scope = new Scope();
            var report = new Report();
            tree.CheckSemantics(scope, report);

            if (!report.IsOk)
            {
                foreach (var item in report)
                    Console.WriteLine("({0},{1}): {2}", item.Line, item.Column, item.Message);

                return 1;
            }

                tree.Compile(path);
                return 0;
            }

            catch (ParsingException e)
            {
                Console.WriteLine("({0},{1}): {2}", e.RecognitionError.Line, e.RecognitionError.CharPositionInLine, e.Message);
                return 1;
            }
            catch (RecognitionException e)
            {
                Console.WriteLine("({0},{1}): {2}", e.Line, e.CharPositionInLine, e.Message);
                return 1;
            }
        }

    }
}
