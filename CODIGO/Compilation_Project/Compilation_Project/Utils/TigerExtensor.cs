﻿using Antlr.Runtime.Tree;
using System.Collections.Generic;
using System.Linq;

namespace Compilation_Project.Utils
{
    internal static class TigerExtensor
    {
        #region ITree

        internal static IEnumerable<T> As<T>(this IList<ITree> nodes)
        {
            if (nodes == null)
                return new List<T>();

            return nodes.Cast<T>();
        }

        #endregion
    }
}
