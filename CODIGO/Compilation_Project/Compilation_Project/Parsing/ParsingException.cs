﻿using System;
using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Parsing
{
    public class ParsingException: Exception
    {
        public ParsingException(string message, Exception innerException)
            : base(message, innerException)
        { }
        public RecognitionException RecognitionError
        {
            get { return InnerException as RecognitionException; }
        }
    }
}
