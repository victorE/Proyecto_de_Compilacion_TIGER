﻿using System.Reflection.Emit;


namespace Compilation_Project.CodeGeneration
{
    public class CodeGenerator
    {
        public TypeBuilder ClassBuilder { get; set; }

        public ILGenerator Generator { get; set; }

        public ModuleBuilder ModuleBuilder { get; set; }
    }
}
