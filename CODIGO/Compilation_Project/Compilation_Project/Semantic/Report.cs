﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Semantic
{
    public class Report : IEnumerable<SemanticInfo>
    {
        #region Constructors

        public Report()
        {
            _Items = new List<SemanticInfo>();
        }

        #endregion

        #region Instances

        private List<SemanticInfo> _Items;

        #endregion

        #region Methods

        public void Add(SemanticInfo item)
        {
            _Items.Add(item);
        }

        #endregion

        #region Properties

        public bool IsOk
        {
            get { return _Items.TrueForAll(item => item.Level != Level.Error); }
        }

        public Level Level { get; set; }

        #endregion

        #region IEnumerable Members

        public IEnumerator<SemanticInfo> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        #endregion
    }

}
