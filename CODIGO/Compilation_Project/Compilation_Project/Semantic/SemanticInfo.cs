﻿using Compilation_Project.AST;
using Compilation_Project.Basic_Types;

namespace Compilation_Project.Semantic
{
    public struct SemanticInfo
    {
        #region Properties

        public int Column
        {
            get { return Node.CharPositionInLine; }
        }

        public Level Level { get; set; }

        public int Line
        {
            get { return Node.Line; }
        }

        public string Message { get; set; }

        public Node Node { get; set; }

        #endregion

        public static SemanticInfo DivisionByZero(Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = "Division by constant zero",
                Node = node
            };
        }

        private static SemanticInfo DuplicatedDeclaration(string typeDeclaration, string name, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Already exist a {0} in current context with name {1}", typeDeclaration, name),
                Node = node
            };
        }

        public static SemanticInfo DuplicatedField(string fieldName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("The field name '{0}' is a duplicate", fieldName),
                Node = node
            };
        }

        public static SemanticInfo DuplicatedRoutine(string functionName, Node node)
        {
            return DuplicatedDeclaration("function or variable", functionName, node);
        }

        public static SemanticInfo DuplicatedType(string typeName, Node node)
        {
            return DuplicatedDeclaration("type", typeName, node);
        }

        public static SemanticInfo DuplicatedVariable(string variableName, Node node)
        {
            return DuplicatedDeclaration("function or variable", variableName, node);
        }

        public static SemanticInfo IncompatibleTypes(string typeExpected, string typeUsed, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Cannot be used an expression of type '{1}' instead of expression of type '{0}'", typeExpected, typeUsed),
                Node = node
            };
        }

        public static SemanticInfo InconsistentAccessibility(string typeName, LanguageNode node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Inconsistent accessibility: '{0}' is not visible in outer scope", typeName),
                Node = node
            };
        }

        public static SemanticInfo InvalidCondition(string typeName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("The condition must be integer, cannot convert '{0}' to '{1}'", typeName, TigerType.Integer.Name),
                Node = node
            };
        }

        public static SemanticInfo InvalidField(string typeName, string fieldName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Type '{0}' does not contain a field named '{1}'", typeName, fieldName),
                Node = node
            };
        }

        public static SemanticInfo InvalidInferredType(string variableName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Invalid inferred type in variable '{0}'", variableName),
                Node = node
            };
        }

        public static SemanticInfo InvalidInteger(string literal, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("'{0}' Integral constant is too large", literal),
                Node = node
            };
        }

        public static SemanticInfo InvalidLength(string typeName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("The length of array must be integer, cannot convert '{0}' to '{1}'", typeName, TigerType.Integer.Name),
                Node = node
            };
        }

        public static SemanticInfo InvalidOperation(string oper, string left, string right, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Operator '{0}' cannot be applied to operands of type '{1}' and '{2}'", oper, left, right),
                Node = node
            };
        }

        public static SemanticInfo InvalidReturn(string statementType, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("The expression '{0}' must not return a value", statementType),
                Node = node
            };
        }

        public static SemanticInfo KeywordUse(string keyword, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Cannot use keyword '{0}' as identifier", keyword),
                Node = node
            };
        }

        public static SemanticInfo LoopBoundsNonInteger(Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = "The loop bounds must be integer expressions",
                Node = node
            };
        }

        public static SemanticInfo NotFoundLoop(Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = "No enclosing loop out of which to break",
                Node = node
            };
        }

        private static SemanticInfo UndefinedReference(string typeReference, string name, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("{0} '{1}' cannot be found in current context", typeReference, name),
                Node = node
            };
        }

        public static SemanticInfo UndefinedFunctionReferenced(string functionName, Node node)
        {
            return UndefinedReference("Function", functionName, node);
        }

        public static SemanticInfo UndefinedTypeReferenced(string typeName, Node node)
        {
            return UndefinedReference("Type", typeName, node);
        }

        public static SemanticInfo UndefinedVariableReferenced(string variableName, Node node)
        {
            return UndefinedReference("Variable", variableName, node);
        }

        private static SemanticInfo UnexpectedType(string typeName, string typeKind, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Type '{0}' isn't {1}", typeName, typeKind),
                Node = node
            };
        }

        public static SemanticInfo UnusedInfo(string typeInfo, string name, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Warning,
                Message = string.Format("The {0} '{1}' is assigned but its value is never used", typeInfo, name),
                Node = node
            };
        }

        public static SemanticInfo UnusedRoutine(string routineName, Node node)
        {
            return UnusedInfo("function", routineName, node);
        }

        public static SemanticInfo UnusedType(string typeName, Node node)
        {
            return UnusedInfo("type", typeName, node);
        }

        public static SemanticInfo UnusedVariable(string variableName, Node node)
        {
            return UnusedInfo("variable", variableName, node);
        }

        public static SemanticInfo NonArrayType(string typeName, Node node)
        {
            return UnexpectedType(typeName, "array", node);
        }

        public static SemanticInfo NonRecordType(string typeName, Node node)
        {
            return UnexpectedType(typeName, "record", node);
        }

        public static SemanticInfo ReadOnlyVariable(string variableName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Readonly variable '{0}' cannot be assigned", variableName),
                Node = node
            };
        }

        public static SemanticInfo TypeCircularDependency(string typeName, Node node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Circular base class dependency involving '{0}'", typeName),
                Node = node
            };
        }

        public static SemanticInfo WrongFieldsNumber(string name, int formalCount, int actualCount, LanguageNode node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Record '{0}' have {1} fields, got {2} instead", name, formalCount, actualCount),
                Node = node
            };
        }

        public static SemanticInfo WrongFieldPosition(string nameExpected, string nameUsed, LanguageNode node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("The field expected is '{0}', got '{1}' instead ", nameExpected, nameUsed),
                Node = node
            };
        }

        public static SemanticInfo WrongParameterNumber(string functionName, int formalCount, int actualCount, LanguageNode node)
        {
            return new SemanticInfo()
            {
                Level = Level.Error,
                Message = string.Format("Function '{0}' takes {1} arguments, got {2} instead", functionName, formalCount, actualCount),
                Node = node
            };
        }
    }

}
