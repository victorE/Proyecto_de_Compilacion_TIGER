﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Semantic
{
    public enum Level
    {
        Info = 0,
        Warning = 1,
        Error = 2
    }
}
