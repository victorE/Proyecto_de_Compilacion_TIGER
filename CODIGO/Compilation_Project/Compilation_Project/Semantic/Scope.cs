﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compilation_Project.Basic_Types;

namespace Compilation_Project.Semantic
{
    public class Scope
    {
        #region Constants

        public static readonly Dictionary<string, RoutineInfo> StandardFunctions;

        public static readonly Dictionary<string, TigerType> StandardTypes;

        #endregion

        #region Static Constructor

        static Scope()
        {
            StandardFunctions = new Dictionary<string, RoutineInfo>();

            InitializeStdFunctions();

            StandardTypes = new Dictionary<string, TigerType>();

            InitializeStdTypes();
        }

        #endregion

        #region Tiger Standard Library

        private static void InitializeStdFunctions()
        {
            // Functions of Tiger Standard Library
            RoutineInfo print = new RoutineInfo()
            {
                IsStandard = true,
                Name = "print",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s", Type = TigerType.String } },
                ReturnType = TigerType.Void
            };
            StandardFunctions.Add(print.Name, print);

            RoutineInfo printi = new RoutineInfo()
            {
                IsStandard = true,
                Name = "printi",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "i", Type = TigerType.Integer } },
                ReturnType = TigerType.Void
            };
            StandardFunctions.Add(printi.Name, printi);

            RoutineInfo ord = new RoutineInfo()
            {
                IsStandard = true,
                Name = "ord",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s", Type = TigerType.String } },
                ReturnType = TigerType.Integer
            };
            StandardFunctions.Add(ord.Name, ord);

            RoutineInfo chr = new RoutineInfo()
            {
                IsStandard = true,
                Name = "chr",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "i", Type = TigerType.Integer } },
                ReturnType = TigerType.String
            };
            StandardFunctions.Add(chr.Name, chr);

            RoutineInfo size = new RoutineInfo()
            {
                IsStandard = true,
                Name = "size",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s", Type = TigerType.String } },
                ReturnType = TigerType.Integer
            };
            StandardFunctions.Add(size.Name, size);

            RoutineInfo substring = new RoutineInfo()
            {
                IsStandard = true,
                Name = "substring",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] {
                    new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s", Type = TigerType.String },
                    new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "f", Type = TigerType.Integer },
                    new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "n", Type = TigerType.Integer }
                },
                ReturnType = TigerType.String
            };
            StandardFunctions.Add(substring.Name, substring);

            RoutineInfo concat = new RoutineInfo()
            {
                IsStandard = true,
                Name = "concat",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] {
                    new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s1", Type = TigerType.String },
                    new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s2", Type = TigerType.String }
                },
                ReturnType = TigerType.String
            };
            StandardFunctions.Add(concat.Name, concat);

            RoutineInfo not = new RoutineInfo()
            {
                IsStandard = true,
                Name = "not",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "i", Type = TigerType.Integer } },
                ReturnType = TigerType.Integer
            };
            StandardFunctions.Add(not.Name, not);

            RoutineInfo exit = new RoutineInfo()
            {
                IsStandard = true,
                Name = "exit",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "i", Type = TigerType.Integer } },
                ReturnType = TigerType.Void
            };
            StandardFunctions.Add(exit.Name, exit);

            RoutineInfo getline = new RoutineInfo()
            {
                IsStandard = true,
                Name = "getline",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { },
                ReturnType = TigerType.String
            };
            StandardFunctions.Add(getline.Name, getline);

            RoutineInfo printline = new RoutineInfo()
            {
                IsStandard = true,
                Name = "printline",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "s", Type = TigerType.String } },
                ReturnType = TigerType.Void
            };
            StandardFunctions.Add(printline.Name, printline);

            RoutineInfo printiline = new RoutineInfo()
            {
                IsStandard = true,
                Name = "printiline",
                Namespace = "TigerStandardLibrary",
                Parameters = new VariableInfo[] { new VariableInfo() { IsParameter = true, IsReadOnly = false, Name = "i", Type = TigerType.Integer } },
                ReturnType = TigerType.Void
            };
            StandardFunctions.Add(printiline.Name, printiline);
        }

        private static void InitializeStdTypes()
        {
            // Types of Tiger Standard Library
            StandardTypes.Add(TigerType.Integer.Name, TigerType.Integer);

            StandardTypes.Add(TigerType.String.Name, TigerType.String);
        }

        #endregion

        #region Constructors

        public Scope(Scope parent = null, string @namespace = "PROGRAM")
        {
            _ChildsNonFunctions = new List<Scope>();

            _CountChilds = 0;

            _SymbolsInstances = new Dictionary<string, TigerInfo>();

            _SymbolsTypes = new Dictionary<string, TigerType>();

            Namespace = @namespace;

            Parent = parent;
        }

        #endregion

        #region Instances

        private List<Scope> _ChildsNonFunctions;
        private int _CountChilds;
        private Dictionary<string, TigerInfo> _SymbolsInstances;
        private Dictionary<string, TigerType> _SymbolsTypes;

        #endregion

        #region Methods

        public Scope CreateChildScope(string newNamespace, bool isVisible)
        {
            Scope newScope = new Scope(this, string.Format("{0}.{1}{2}", Namespace, ++_CountChilds, newNamespace));

            if (isVisible)
                _ChildsNonFunctions.Add(newScope);

            return newScope;
        }

        #endregion

        #region Properties

        public Scope Parent { get; private set; }

        public string Namespace { get; private set; }

        #endregion

        #region Manage Routines

        public void DefineRoutine(RoutineInfo routine)
        {
            if (_SymbolsInstances.ContainsKey(routine.Name))
                throw new Exception("Existe una variable o funcion de igual nombre.");

            routine.Namespace = Namespace;
            _SymbolsInstances.Add(routine.Name, routine);
        }

        public bool ExistRoutine(string routineName)
        {
            TigerInfo value;
            if (_SymbolsInstances.TryGetValue(routineName, out value))
                return value is RoutineInfo;

            return Parent != null && Parent.ExistRoutine(routineName);
        }

        public bool ExistRoutineInCurrentContext(string routineName)
        {
            if (StandardFunctions.ContainsKey(routineName))
                return true;

            return _SymbolsInstances.ContainsKey(routineName);
        }

        public IEnumerable<RoutineInfo> GetUnusedRoutines()
        {
            foreach (var unusedRoutine in _SymbolsInstances.Values.OfType<RoutineInfo>().Where(routine => !routine.IsUsed))
                yield return unusedRoutine;
        }

        public bool TryGetRoutine(string name, out RoutineInfo routine)
        {
            if (StandardFunctions.TryGetValue(name, out routine))
                return true;

            TigerInfo value;
            if (_SymbolsInstances.TryGetValue(name, out value))
            {
                routine = value as RoutineInfo;
                return routine != null;
            }

            routine = null;
            return Parent != null && Parent.TryGetRoutine(name, out routine);
        }

        #endregion

        #region Manage Types

        public void DefineType(TigerType type)
        {
            if (_SymbolsTypes.ContainsKey(type.Name))
                throw new Exception("Existe un tipo de igual nombre.");

            type.Namespace = Namespace;
            _SymbolsTypes.Add(type.Name, type);
        }

        public bool ExistType(string typeName)
        {
            return _SymbolsTypes.ContainsKey(typeName) || (Parent != null && Parent.ExistType(typeName));
        }

        public bool ExistTypeInCurrentContext(string typeName)
        {
            if (StandardTypes.ContainsKey(typeName))
                return true;

            return _SymbolsTypes.ContainsKey(typeName);
        }

        public IEnumerable<TigerType> GetUnusedTypes()
        {
            foreach (var unusedType in _SymbolsTypes.Values.OfType<TigerType>().Where(type => !type.IsUsed))
                yield return unusedType;
        }

        public void RemoveInconsistentType(string typeName)
        {
            _SymbolsTypes.Remove(typeName);
        }

        public void ResolveType(string typeName, TigerType realType)
        {
            _SymbolsTypes[typeName] = realType;
        }

        public bool TryGetType(string name, out TigerType type)
        {
            if (StandardTypes.TryGetValue(name, out type))
                return true;

            if (_SymbolsTypes.TryGetValue(name, out type))
                return true;

            return Parent != null && Parent.TryGetType(name, out type);
        }

        #endregion

        #region Manage Variables

        public void DefineVariable(VariableInfo variable)
        {
            if (_SymbolsInstances.ContainsKey(variable.Name))
                throw new Exception("Existe una variable o funcion de igual nombre.");

            variable.Namespace = Namespace;
            _SymbolsInstances.Add(variable.Name, variable);
        }

        public bool ExistVariable(string variableName)
        {
            TigerInfo value;
            if (_SymbolsInstances.TryGetValue(variableName, out value))
                return value is VariableInfo;

            return Parent != null && Parent.ExistVariable(variableName);
        }

        public bool ExistVariableInCurrentContext(string variableName)
        {
            if (StandardFunctions.ContainsKey(variableName))
                return true;

            return _SymbolsInstances.ContainsKey(variableName);
        }

        public IEnumerable<VariableInfo> GetInnerVariables()
        {
            foreach (var varInfo in _SymbolsInstances.Values.OfType<VariableInfo>().Where(variable => variable.IsUsed))
                yield return varInfo;

            foreach (var scope in _ChildsNonFunctions)
                foreach (var varInfo in scope.GetInnerVariables())
                    yield return varInfo;
        }

        public IEnumerable<VariableInfo> GetUnusedVariables()
        {
            foreach (var unusedVariable in _SymbolsInstances.Values.OfType<VariableInfo>().Where(variable => !variable.IsUsed))
                yield return unusedVariable;
        }

        public bool TryGetVariable(string name, out VariableInfo variable)
        {
            TigerInfo value;
            if (_SymbolsInstances.TryGetValue(name, out value))
            {
                variable = value as VariableInfo;
                return variable != null;
            }

            variable = null;
            return Parent != null && Parent.TryGetVariable(name, out variable);
        }

        #endregion
    }

}
