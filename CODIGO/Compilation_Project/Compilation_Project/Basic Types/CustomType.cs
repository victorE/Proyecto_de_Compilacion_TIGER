﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Basic_Types
{
    public abstract class CustomType : TigerType
    {
        public CustomType(string name, string @namespace = "")
            : base(name, @namespace)
        { }
    }
}
