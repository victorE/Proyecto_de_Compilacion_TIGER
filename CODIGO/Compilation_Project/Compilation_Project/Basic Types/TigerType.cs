﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.Basic_Types.Types;

namespace Compilation_Project.Basic_Types
{
        public abstract class TigerType : TigerInfo
        {
            #region Constants

            public static readonly TigerType Error = new ErrorType();

            public static readonly TigerType Integer = new IntegerType();

            public static readonly TigerType Nil = new NilType();

            public static readonly TigerType String = new StringType();

            public static readonly TigerType Void = new VoidType();

            #endregion

            #region Constructors

            public TigerType(string name, string @namespace)
            {
                Name = name;
                Namespace = @namespace;
            }

            #endregion

            #region Methods

            public static bool AreCompatibles(TigerType a, TigerType b)
            {
                // si son iguales obviamente son compatibles...
                if (a == b)
                    return true;

                // si alguno es nullable y el otro es nil son compatibles...
                if ((a.IsNullable && b == Nil) ||
                    (b.IsNullable && a == Nil))
                    return true;

                return false;
            }

            public override bool Equals(object obj)
            {
                return base.Equals(obj);
            }

            public abstract Type GetCILType(ModuleBuilder mb);

            public override int GetHashCode()
            {
                return FullName.GetHashCode();
            }

            #endregion

            #region Properties

            public abstract bool IsNullable { get; }

            #endregion

            #region Operators

            public static bool operator ==(TigerType t1, TigerType t2)
            {
                return object.Equals(t1, t2);
            }

            public static bool operator !=(TigerType t1, TigerType t2)
            {
                return !(t1 == t2);
            }

            #endregion
        }
}
