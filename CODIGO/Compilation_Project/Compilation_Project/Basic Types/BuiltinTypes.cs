﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Basic_Types
{
    public abstract class BuiltinType : TigerType
    {
        public BuiltinType(string name)
            : base(name, "TigerStandardLibrary")
        { }
    }
}
