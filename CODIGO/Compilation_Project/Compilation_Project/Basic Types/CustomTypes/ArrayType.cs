﻿using System;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Basic_Types.CustomTypes
{
    public class ArrayType : CustomType
    {
        #region Constructors

        public ArrayType(string name, string @namespace = "")
            : base(name, @namespace)
        { }

        #endregion

        #region Methods

        public override Type GetCILType(ModuleBuilder mb)
        {
            return ElementsType.GetCILType(mb).MakeArrayType();
        }

        #endregion

        #region Properties

        public TigerType ElementsType { get; set; }

        public override bool IsNullable
        {
            get { return true; }
        }

        #endregion
    }

}
