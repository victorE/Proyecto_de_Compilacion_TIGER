﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Compilation_Project.Basic_Types.CustomTypes
{
    public class RecordType : CustomType
    {
        #region Constructors

        public RecordType(string name, string @namespace = "")
            : base(name, @namespace)
        { }

        #endregion

        #region Instances

        private TypeBuilder _Type;

        #endregion

        #region Methods

        public override Type GetCILType(ModuleBuilder mb)
        {
            if (_Type != null)
                return _Type;

            _Type = mb.DefineType(
                FullName,
                TypeAttributes.Public
                );

            // building fields...
            foreach (var field in Fields)
                field.GeneratedField = _Type.DefineField(
                    field.Name,
                    field.Type.GetCILType(mb),
                    FieldAttributes.Public
                    );

            // building ctor...
            ConstructorBuilder ctor = _Type.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                System.Type.EmptyTypes
                );

            ConstructorInfo baseCtor = typeof(object).GetConstructor(System.Type.EmptyTypes);
            ILGenerator ctorGenerator = ctor.GetILGenerator();
            ctorGenerator.Emit(OpCodes.Ldarg_0);
            ctorGenerator.Emit(OpCodes.Call, baseCtor);
            ctorGenerator.Emit(OpCodes.Ret);

            _Type.CreateType();
            return _Type;
        }

        #endregion

        #region Properties

        public List<Field> Fields { get; set; }

        public override bool IsNullable
        {
            get { return true; }
        }

        #endregion
    }

}
