﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection.Emit;

namespace Compilation_Project.Basic_Types.CustomTypes
{
    public class Field
    {
        #region Properties

        public FieldBuilder GeneratedField { get; set; }

        public string Name { get; set; }

        public TigerType Type { get; set; }

        #endregion
    }
}
