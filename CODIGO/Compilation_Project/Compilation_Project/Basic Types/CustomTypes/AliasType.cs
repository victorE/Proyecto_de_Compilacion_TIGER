﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;
using System.Threading.Tasks;

namespace Compilation_Project.Basic_Types.CustomTypes
{
    public class AliasType : CustomType
    {
        #region Constructors

        public AliasType(string name, string @namespace = "")
            : base(name, @namespace)
        { }

        #endregion

        #region Methods

        public override Type GetCILType(ModuleBuilder mb)
        {
            return BaseType.GetCILType(mb);
        }

        #endregion

        #region Properties

        public TigerType BaseType { get; set; }

        public override bool IsNullable
        {
            get { return BaseType.IsNullable; }
        }

        #endregion
    }

}
