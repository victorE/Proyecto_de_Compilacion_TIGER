﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.Basic_Types
{
    public class VariableInfo : TigerInfo
    {
        #region Properties

        public FieldBuilder GeneratedVariable { get; set; }

        public bool IsParameter { get; set; }

        public bool IsReadOnly { get; set; }

        public TigerType Type { get; set; }

        #endregion

        #region Methods

        public void CreateField(CodeGenerator cg)
        {
            if (GeneratedVariable == null)
            {
                GeneratedVariable = cg.ClassBuilder.DefineField(
                    FullName,
                    Type.GetCILType(cg.ModuleBuilder),
                    FieldAttributes.Public | FieldAttributes.Static
                    );
            }
        }

        public void LoadValueToStack(CodeGenerator cg)
        {
            cg.Generator.Emit(OpCodes.Ldsfld, GeneratedVariable);
        }

        public void SetValueFromStack(CodeGenerator cg)
        {
            cg.Generator.Emit(OpCodes.Stsfld, GeneratedVariable);
        }

        #endregion
    }

}
