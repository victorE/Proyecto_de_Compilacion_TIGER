﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compilation_Project.Basic_Types
{
    public abstract class TigerInfo
    {
       
        public string FullName
        {
            get { return string.Format("{0}.{1}", Namespace, Name); }
        }

        public bool IsUsed { get; set; }

        public string Name { get; set; }

        public string Namespace { get; set; }

    }
}
