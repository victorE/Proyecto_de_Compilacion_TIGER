﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Compilation_Project.Basic_Types
{
    public class RoutineInfo : TigerInfo
    {
        #region Properties

        public MethodInfo GeneratedMethod { get; set; }

        public bool IsStandard { get; set; }

        public VariableInfo[] Parameters { get; set; }

        public int ParameterCount
        {
            get { return Parameters.Length; }
        }

        public TigerType ReturnType { get; set; }

        #endregion
    }
}
