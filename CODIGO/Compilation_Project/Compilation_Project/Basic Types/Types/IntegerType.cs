﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection.Emit;

namespace Compilation_Project.Basic_Types.Types
{
    public class IntegerType : BuiltinType
    {
        #region Constructors

        public IntegerType(): base("int") { }

        #endregion

        #region Methods

        public override Type GetCILType(ModuleBuilder mb)
        {
            return typeof(int);
        }

        #endregion

        #region Properties

        public override bool IsNullable
        {
            get { return false; }
        }

        #endregion
    }
}
