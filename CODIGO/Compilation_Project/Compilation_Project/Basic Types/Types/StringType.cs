﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection.Emit;

namespace Compilation_Project.Basic_Types.Types
{
    public class StringType : BuiltinType
    {
        #region Constructors

        public StringType()
            : base("string")
        { }

        #endregion

        #region Methods

        public override Type GetCILType(ModuleBuilder mb)
        {
            return typeof(string);
        }

        #endregion

        #region Properties

        public override bool IsNullable
        {
            get { return true; }
        }

        #endregion
    }

}
