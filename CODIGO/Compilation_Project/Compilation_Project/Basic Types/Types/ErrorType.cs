﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.Basic_Types.Types
{
    public class ErrorType : TigerType
    {
        #region Constructors

        public ErrorType(): base("Error", ""){ }

        #endregion

        #region Methods

        public override Type GetCILType(ModuleBuilder mb)
        {
            throw new InvalidOperationException();
        }

        #endregion

        #region Properties

        public override bool IsNullable
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
