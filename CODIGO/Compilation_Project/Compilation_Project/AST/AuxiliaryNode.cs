﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;

namespace Compilation_Project.AST
{
    public class AuxiliaryNode:Node
    {
        protected AuxiliaryNode() : base() { }

        protected AuxiliaryNode(IToken token) : base(token) { }

        protected AuxiliaryNode(AuxiliaryNode node) : base(node) { }
    }
}
