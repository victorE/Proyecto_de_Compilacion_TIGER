﻿using Antlr.Runtime;

namespace Compilation_Project.AST
{
    public abstract class BasicNode : ExpressionNode
    {

        public BasicNode() : base() { }

        public BasicNode(IToken token) : base(token) { }

        public BasicNode(BasicNode node) : base(node) { }

    }
}