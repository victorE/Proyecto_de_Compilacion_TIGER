﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Compilation_Project.AST.BasicNodes; 

namespace Compilation_Project.AST
{
    public abstract class Node:CommonTree
    {
        protected Node() : base() { }

        protected Node(Node node) : base(node) { }

        protected Node(IToken token) : base(token) { }

        public override bool IsNil
        {
            get
            {
                return (base.IsNil && (this is NilNode));
            }
        }

        public override ITree DupNode()
        {
            return (Node)Activator.CreateInstance(this.GetType(), this);
        }
    }
}
