﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public abstract class BinaryExpressionNode:ExpressionNode
    {
        public BinaryExpressionNode() : base() { }

        public BinaryExpressionNode(IToken token) : base(token) { }

        public BinaryExpressionNode(BinaryExpressionNode node) : base(node) { }

        public ExpressionNode LeftExpression
        {
            get { return Children[0] as ExpressionNode; }
        }
        public ExpressionNode RightExpression
        {
            get { return Children[1] as ExpressionNode; }
        }

        //para ejecutar la expression dependiendo de que sea
        public abstract OpCode OperatorOpCode { get; }

        public override void CheckSemantics(Scope scope, Report errors)
        {
            LeftExpression.CheckSemantics(scope, errors);
            RightExpression.CheckSemantics(scope, errors);

            // verificar si los operadores estan bien.
            if (!LeftExpression.IsOk || !RightExpression.IsOk)
                ExpressionType = TigerType.Error;
        }

    }
}
