﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using Compilation_Project.Semantic;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.AST
{
    public abstract class LanguageNode : Node
    {
        protected LanguageNode() : base() { }
        protected LanguageNode(IToken token) : base(token) { }
        protected LanguageNode(LanguageNode node) : base(node) { }


        public abstract bool IsOk { get; }
        public abstract void CheckSemantics(Scope scope, Report errors);
        public abstract void GenerateCode(CodeGenerator cg);
    }
}
