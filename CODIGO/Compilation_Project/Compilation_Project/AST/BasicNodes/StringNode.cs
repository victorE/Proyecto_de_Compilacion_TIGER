﻿using Antlr.Runtime;
using System.Reflection.Emit;
using System.Text;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.BasicNodes
{
    public class StringNode : BasicNode
    {
        #region Constructors

        public StringNode() : base() { }

        public StringNode(IToken token) : base(token) { }

        public StringNode(IntNode node) : base(node) { }

        #endregion

        public string Value { get; set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            ExpressionType = TigerType.String;
            StringBuilder sb = new StringBuilder();

            for (int i = 1; i < Text.Length - 1; i++)
            {
                if (Text[i] == '\\')
                {
                    switch (Text[++i])
                    {
                        case 'n':
                            sb.Append('\n');
                            break;
                        case 'r':
                            sb.Append('\r');
                            break;
                        case 't':
                            sb.Append('\t');
                            break;
                        case '"':
                            sb.Append('\"');
                            break;
                        case '\\':
                            sb.Append('\\');
                            break;
                        default:
                            if (char.IsDigit(Text[i]))
                            {
                                int chr = (Text[i++] - '0') * 100 + (Text[i++] - '0') * 10 + Text[i] - '0';
                                sb.Append((char)chr);
                            }
                            else
                                while (Text[++i] != '\\') ;
                            break;
                    }
                }
                else
                    sb.Append(Text[i]);
            }

            Value = sb.ToString();
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            cg.Generator.Emit(OpCodes.Ldstr, Value);
        }
    }

}
