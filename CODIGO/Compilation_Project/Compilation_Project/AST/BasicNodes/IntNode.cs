﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.AST.BasicNodes
{
    public class IntNode : BasicNode
    {
        #region Constructors

        public IntNode() : base() { }

        public IntNode(IToken token) : base(token) { }

        public IntNode(IntNode node) : base(node) { }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            int result = 0;
            if (!int.TryParse(this.Text, out result))
            {
                report.Add(SemanticInfo.InvalidInteger(Text, this));
                ExpressionType = TigerType.Error;
            }
            else
                ExpressionType = TigerType.Integer;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            cg.Generator.Emit(OpCodes.Ldc_I4, int.Parse(Text));
        }
    }

}
