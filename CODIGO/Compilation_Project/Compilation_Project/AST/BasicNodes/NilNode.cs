﻿using System;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.BasicNodes
{
    public class NilNode : BasicNode
    {
        #region Constructors

        public NilNode() : base() { }

        public NilNode(IToken token) : base(token) { }

        public NilNode(NilNode node) : base(node) { }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            ExpressionType = TigerType.Nil;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            cg.Generator.Emit(OpCodes.Ldnull);
        }
    }

}
