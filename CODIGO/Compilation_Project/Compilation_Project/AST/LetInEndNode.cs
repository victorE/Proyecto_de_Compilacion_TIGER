﻿using Antlr.Runtime;
using Compilation_Project.AST.Declaration;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class LetInEndNode : ExpressionNode
    {
        #region Constructors

        public LetInEndNode() : base() { }

        public LetInEndNode(IToken token) : base(token) { }

        public LetInEndNode(LetInEndNode node) : base(node) { }

        #endregion

        public DeclarationListNode Declarations
        {
            get { return Children[0] as DeclarationListNode; }
        }

        public ExpressionSequenceNode Expressions
        {
            get { return Children[1] as ExpressionSequenceNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            var letScope = scope.CreateChildScope("LET", true);

            // Check childrens...
            Declarations.CheckSemantics(letScope, report);
            Expressions.CheckSemantics(letScope, report);

            // Warnings...
            foreach (var unusedRoutine in letScope.GetUnusedRoutines())
                report.Add(SemanticInfo.UnusedRoutine(unusedRoutine.Name, this));

            foreach (var unusedType in letScope.GetUnusedTypes())
                report.Add(SemanticInfo.UnusedType(unusedType.Name, this));

            foreach (var unusedVariable in letScope.GetUnusedVariables())
                report.Add(SemanticInfo.UnusedVariable(unusedVariable.Name, this));

            if (!Declarations.IsOk || !Expressions.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Preprocessing...
            var type = Expressions.ExpressionType;

            // Type-checking expression...
            TigerType expectedType;
            if (type != TigerType.Void && type != TigerType.Nil &&
               (!scope.TryGetType(type.Name, out expectedType) || expectedType != type))
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.InconsistentAccessibility(type.Name, this));
                return;
            }

            // Ok
            ExpressionType = type;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            Declarations.GenerateCode(cg);
            Expressions.GenerateCode(cg);
        }
    }

}
