﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;


namespace Compilation_Project.AST.LValue
{
    public class ValueNode : LValueNode
    {
        #region Constructors

        public ValueNode() : base() { }

        public ValueNode(IToken token) : base(token) { }

        public ValueNode(ValueNode node) : base(node) { }

        #endregion

        public string VariableName
        {
            get { return (Children[0] as IdNode).Name; }
        }

        public VariableInfo ReferencedVariable { get; private set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking variable existence...
            VariableInfo value;
            if (!scope.TryGetVariable(VariableName, out value))
            {
                report.Add(SemanticInfo.UndefinedVariableReferenced(VariableName, this));
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ReferencedVariable = value;
            ReferencedVariable.IsUsed = true;
            ExpressionType = ReferencedVariable.Type;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            ReferencedVariable.LoadValueToStack(cg);
        }

        public override void Load(CodeGenerator cg)
        {
            // do nothing...
        }

        public override void Set(CodeGenerator cg)
        {
            ReferencedVariable.SetValueFromStack(cg);
        }
    }

}
