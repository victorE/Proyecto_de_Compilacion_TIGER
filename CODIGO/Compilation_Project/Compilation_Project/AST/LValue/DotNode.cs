﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.LValue
{
    public class DotNode : LValueNode
    {
        #region Constructors

        public DotNode() : base() { }

        public DotNode(IToken token) : base(token) { }

        public DotNode(DotNode node) : base(node) { }

        #endregion

        public LValueNode Variable
        {
            get { return Children[0] as LValueNode; }
        }

        public string FieldName
        {
            get { return (Children[1] as IdNode).Text; }
        }

        public Field FieldUsed { get; set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking children...
            Variable.CheckSemantics(scope, report);
            if (!Variable.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Type-checking variable...
            RecordType type = Variable.ExpressionType as RecordType;
            if (type == null)
            {
                report.Add(SemanticInfo.NonRecordType(Variable.ExpressionType.Name, this));
                ExpressionType = TigerType.Error;
                return;
            }

            // Checking field existence...
            FieldUsed = type.Fields.FirstOrDefault(field => field.Name == FieldName);
            if (FieldUsed == null)
            {
                report.Add(SemanticInfo.InvalidField(type.Name, FieldName, Children[1] as Node));
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = FieldUsed.Type;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Load variable...
            Load(cg);

            // Load record's field...
            cg.Generator.Emit(OpCodes.Ldfld, FieldUsed.GeneratedField);
        }

        public override void Load(CodeGenerator cg)
        {
            // Load left value...
            Variable.GenerateCode(cg);
        }

        public override void Set(CodeGenerator cg)
        {
            // Set record's field...
            cg.Generator.Emit(OpCodes.Stfld, FieldUsed.GeneratedField);
        }
    }

}
