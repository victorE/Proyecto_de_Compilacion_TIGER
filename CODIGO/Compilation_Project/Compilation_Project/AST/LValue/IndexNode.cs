﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.LValue
{
    
    public class IndexNode : LValueNode
    {
        #region Constructors

        public IndexNode() : base() { }

        public IndexNode(IToken token) : base(token) { }

        public IndexNode(IndexNode node) : base(node) { }

        #endregion

        public LValueNode Variable
        {
            get { return Children[0] as LValueNode; }
        }

        public ExpressionNode Index
        {
            get { return Children[1] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking children variable...
            Variable.CheckSemantics(scope, report);
            if (!Variable.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Type-checking variable...
            ArrayType type = Variable.ExpressionType as ArrayType;
            if (type == null)
            {
                report.Add(SemanticInfo.NonArrayType(Variable.ExpressionType.Name, this));
                ExpressionType = TigerType.Error;
                return;
            }

            // Checking children index...
            Index.CheckSemantics(scope, report);
            if (!Index.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Type-checking index...
            if (!TigerType.AreCompatibles(Index.ExpressionType, TigerType.Integer))
            {
                report.Add(SemanticInfo.IncompatibleTypes(TigerType.Integer.Name, Index.ExpressionType.Name, this));
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = type.ElementsType;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Load variable...
            Load(cg);

            // Load array's element...
            cg.Generator.Emit(OpCodes.Ldelem, (Variable.ExpressionType as ArrayType).ElementsType.GetCILType(cg.ModuleBuilder));
        }

        public override void Load(CodeGenerator cg)
        {
            // Load left value...
            Variable.GenerateCode(cg);

            // Load array's index...
            Index.GenerateCode(cg);
        }

        public override void Set(CodeGenerator cg)
        {
            // Set array's element...
            cg.Generator.Emit(OpCodes.Stelem, (Variable.ExpressionType as ArrayType).ElementsType.GetCILType(cg.ModuleBuilder));
        }
    }

}
