﻿using Antlr.Runtime;
using Antlr.Runtime.Tree;
using System.Reflection.Emit;
using Compilation_Project.AST.Declaration;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Statement
{
    public class BreakNode : StatementNode
    {
        #region Constructors

        public BreakNode() : base() { }

        public BreakNode(IToken token) : base(token) { }

        public BreakNode(BreakNode node) : base(node) { }

        #endregion

        public IBreakableNode Owner { get; private set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            ITree tree = this.Parent;
            while (tree != null && !(tree is IBreakableNode))
            {
                if (tree is DeclarationFunctionNode)
                    break;
               
                if (tree is ExpressionSequenceNode)
                    (tree as ExpressionSequenceNode).ExpressionType = TigerType.Void;

                tree = tree.Parent;
            }

            if (tree == null || !(tree is IBreakableNode))
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.NotFoundLoop(this));
                return;
            }

            // Ok
            Owner = tree as IBreakableNode;
            ExpressionType = TigerType.Void;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            cg.Generator.Emit(OpCodes.Br, Owner.EndLoop);
        }
    }

}
