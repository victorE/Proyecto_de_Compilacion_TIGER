﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Statement
{
    public class ForNode : StatementNode, IBreakableNode
    {
        #region Constructors

        public ForNode() : base() { }

        public ForNode(IToken token) : base(token) { }

        public ForNode(ForNode node) : base(node) { }

        #endregion

        public ExpressionNode Body
        {
            get { return Children[3] as ExpressionNode; }
        }

        public Label EndLoop { get; private set; }

        public ExpressionNode From
        {
            get { return Children[1] as ExpressionNode; }
        }

        public string LoopIndexName
        {
            get { return (Children[0] as IdNode).Text; }
        }

        public VariableInfo LoopIndexInfo { get; set; }

        public ExpressionNode To
        {
            get { return Children[2] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            From.CheckSemantics(scope, report);
            To.CheckSemantics(scope, report);

            LoopIndexInfo = new VariableInfo()
            {
                IsParameter = false,
                IsReadOnly = true,
                Name = LoopIndexName,
                Type = TigerType.Integer
            };

            var bodyScope = scope.CreateChildScope("FOR", true);
            bodyScope.DefineVariable(LoopIndexInfo);

            Body.CheckSemantics(bodyScope, report);

            // Checking childrens...
            if (!From.IsOk || !To.IsOk || !Body.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = TigerType.Void;

            // Type-checking loop bounds...
            if (From.ExpressionType != TigerType.Integer || To.ExpressionType != TigerType.Integer)
            {

                report.Add(SemanticInfo.LoopBoundsNonInteger(this));
                ExpressionType = TigerType.Error;
            }

            // Type-checking body...
            if (Body.ExpressionType != TigerType.Void)
            {
                report.Add(SemanticInfo.InvalidReturn("for", this));
                ExpressionType = TigerType.Error;
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Define loop index...
            LoopIndexInfo.CreateField(cg);

            // Defining labels...
            Label beginLoop = cg.Generator.DefineLabel();
            EndLoop = cg.Generator.DefineLabel();

            cg.Generator.BeginScope();

            LocalBuilder upper = cg.Generator.DeclareLocal(To.ExpressionType.GetCILType(cg.ModuleBuilder));

            // Generate lower bound...
            From.GenerateCode(cg);
            LoopIndexInfo.SetValueFromStack(cg);

            // Generate upper bound...
            To.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Stloc, upper);

            // Beginning of body...
            cg.Generator.MarkLabel(beginLoop);

            // Test upper bounds...
            LoopIndexInfo.LoadValueToStack(cg);
            cg.Generator.Emit(OpCodes.Ldloc, upper);
            cg.Generator.Emit(OpCodes.Bgt, EndLoop);

            // Generate body...
            Body.GenerateCode(cg);

            // Increment loop index...
            LoopIndexInfo.LoadValueToStack(cg);
            cg.Generator.Emit(OpCodes.Ldc_I4_1);
            cg.Generator.Emit(OpCodes.Add);
            LoopIndexInfo.SetValueFromStack(cg);

            cg.Generator.Emit(OpCodes.Br, beginLoop);

            // End of body...
            cg.Generator.MarkLabel(EndLoop);

            cg.Generator.EndScope();
        }
    }
}
