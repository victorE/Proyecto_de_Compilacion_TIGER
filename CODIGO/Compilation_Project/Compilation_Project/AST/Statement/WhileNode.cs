﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;


namespace Compilation_Project.AST.Statement
{
    public class WhileNode : StatementNode, IBreakableNode
    {
        #region Constructors

        public WhileNode() : base() { }

        public WhileNode(IToken token) : base(token) { }

        public WhileNode(WhileNode node) : base(node) { }

        #endregion

        public ExpressionNode Condition
        {
            get { return Children[0] as ExpressionNode; }
        }

        public ExpressionNode Body
        {
            get { return Children[1] as ExpressionNode; }
        }

        public Label EndLoop { get; private set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking childrens...
            Condition.CheckSemantics(scope, report);
            Body.CheckSemantics(scope, report);
            if (!Condition.IsOk || !Body.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = TigerType.Void;

            // Type-checking condition...
            if (Condition.ExpressionType != TigerType.Integer)
            {
                report.Add(SemanticInfo.IncompatibleTypes(TigerType.Integer.Name, Condition.ExpressionType.Name, Condition));
                ExpressionType = TigerType.Error;
            }

            // Type-checking body...
            if (Body.ExpressionType != TigerType.Void)
            {
                report.Add(SemanticInfo.InvalidReturn("while", this));
                ExpressionType = TigerType.Error;
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Defining labels...
            Label startLoop = cg.Generator.DefineLabel();
            EndLoop = cg.Generator.DefineLabel();

            // Beginning of body...
            cg.Generator.MarkLabel(startLoop);

            // Test condition...
            Condition.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Brfalse, EndLoop);

            // Generate body...
            Body.GenerateCode(cg);

            cg.Generator.Emit(OpCodes.Br, startLoop);

            // End of body...
            cg.Generator.MarkLabel(EndLoop);
        }
    }

}
