﻿using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.AST.Statement
{
    public class IfThenNode : StatementNode
    {
        #region Constructors

        public IfThenNode() : base() { }

        public IfThenNode(IToken token) : base(token) { }

        public IfThenNode(IfThenNode node) : base(node) { }

        #endregion

        public ExpressionNode Condition
        {
            get { return Children[0] as ExpressionNode; }
        }

        public ExpressionNode Then
        {
            get { return Children[1] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking childrens...
            Condition.CheckSemantics(scope, report);
            Then.CheckSemantics(scope, report);
            if (!Condition.IsOk || !Then.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = TigerType.Void;

            // Type-checking condition...
            if (Condition.ExpressionType != TigerType.Integer)
            {
                report.Add(SemanticInfo.InvalidCondition(Condition.ExpressionType.Name, Condition));
                ExpressionType = TigerType.Error;
            }

            // Type-checking then...
            if (Then.ExpressionType != TigerType.Void)
            {
                report.Add(SemanticInfo.InvalidReturn("if then", this));
                ExpressionType = TigerType.Error;
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            Label end = cg.Generator.DefineLabel();

            // Generate & test condition...
            Condition.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Brfalse, end);

            // Generate then body...
            Then.GenerateCode(cg);

            cg.Generator.MarkLabel(end);
        }
    }

}
