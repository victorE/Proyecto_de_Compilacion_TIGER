﻿using Antlr.Runtime;
using Compilation_Project.AST.LValue;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Statement
{
    public class AssignNode : StatementNode
    {
        #region Constructors

        public AssignNode() : base() { }

        public AssignNode(IToken token) : base(token) { }

        public AssignNode(AssignNode node) : base(node) { }

        #endregion

        public LValueNode Variable
        {
            get { return Children[0] as LValueNode; }
        }

        public ExpressionNode Value
        {
            get { return Children[1] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking childrens...
            Variable.CheckSemantics(scope, report);
            Value.CheckSemantics(scope, report);
            if (!Variable.IsOk || !Value.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Checking readonly variables...
            if (Variable is ValueNode && (Variable as ValueNode).ReferencedVariable.IsReadOnly)
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.ReadOnlyVariable((Variable as ValueNode).VariableName, this));
                return;
            }

            // Type-checking variable...
            if (!TigerType.AreCompatibles(Variable.ExpressionType, Value.ExpressionType))
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.IncompatibleTypes(Variable.ExpressionType.Name, Value.ExpressionType.Name, this));
                return;
            }

            // Ok
            ExpressionType = TigerType.Void;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Load left value in stack...
            Variable.Load(cg);

            // Generate value to assign...
            Value.GenerateCode(cg);

            // Set left value...
            Variable.Set(cg);
        }
    }

}
