﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;
using Antlr.Runtime;
using Compilation_Project.Basic_Types;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;

namespace Compilation_Project.AST
{
    public class ProgramNode : LanguageNode
    {
        public ProgramNode() : base() { }

        public ProgramNode(IToken token) : base(token) { }

        public ProgramNode(ProgramNode node) : base(node) { }

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public ExpressionNode Program
        {
            get { return this.Children[0] as ExpressionNode; }
        }


        public override void CheckSemantics(Scope scope, Report errors)
        {
            //Vamos a revisar la semantica de los hijos
            Program.CheckSemantics(scope, errors);
            if (!Program.IsOk)
            {
                _IsOk = false;
                return;
            }

            // Type-checking...
            if (!(Program.ExpressionType is BuiltinType))
            {
                errors.Add(new SemanticInfo()
                {
                    Level = Level.Error,
                    Message = "The return type of program must be 'int' or 'string' or 'nil' or 'void'",
                    Node = this
                });
                _IsOk = false;
                return;
            }

            // Ok
            _IsOk = true;
            return;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            GenerateStandardLibrary(cg);

            Program.GenerateCode(cg);
        }

        public void Compile(string path)
        {
            string name = Path.GetFileNameWithoutExtension(path);
            string filename = name + ".exe";

            AssemblyName assemblyName = new AssemblyName(name);
            AssemblyBuilder assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave, Path.GetDirectoryName(Path.GetFullPath(path)));

            ModuleBuilder moduleBuilder = assembly.DefineDynamicModule(name, filename);
            TypeBuilder programBuilder = moduleBuilder.DefineType("Program");
            MethodBuilder mainMethod = programBuilder.DefineMethod(
                "Main",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(void),
                System.Type.EmptyTypes
                );

            assembly.SetEntryPoint(mainMethod);

            ILGenerator generator = mainMethod.GetILGenerator();

            generator.BeginExceptionBlock();
            CodeGenerator cg = new CodeGenerator()
            {
                ClassBuilder = programBuilder,
                Generator = generator,
                ModuleBuilder = moduleBuilder
            };

            GenerateCode(cg);

            if (Program.ExpressionType != TigerType.Void)
                generator.Emit(OpCodes.Pop);

            generator.Emit(OpCodes.Ldc_I4_0);
            generator.Emit(OpCodes.Call, typeof(Environment).GetMethod("Exit"));

            generator.BeginCatchBlock(typeof(Exception));

            generator.BeginScope();

            // Defining exception...
            LocalBuilder exception = generator.DeclareLocal(typeof(Exception));
            MethodInfo writeLineSO = typeof(System.IO.TextWriter).GetMethod("WriteLine", new Type[] { typeof(string), typeof(object) });
            MethodInfo writeLineS = typeof(System.IO.TextWriter).GetMethod("WriteLine", new Type[] { typeof(string) });
            MethodInfo standardErrorOutput = typeof(Console).GetProperty("Error").GetGetMethod();
            generator.Emit(OpCodes.Stloc, exception);

            // Write head...
            generator.Emit(OpCodes.Call, standardErrorOutput);
            generator.Emit(OpCodes.Ldstr, "Exception of type '{0}' was thrown.");
            generator.Emit(OpCodes.Ldloc, exception);
            generator.Emit(OpCodes.Callvirt, typeof(Exception).GetMethod("GetType", System.Type.EmptyTypes));
            generator.Emit(OpCodes.Callvirt, typeof(Type).GetProperty("Name").GetGetMethod());
            generator.Emit(OpCodes.Callvirt, writeLineSO);

            // Write Message to Standard Output Error...
            generator.Emit(OpCodes.Call, standardErrorOutput);
            generator.Emit(OpCodes.Ldloc, exception);
            generator.Emit(OpCodes.Callvirt, typeof(Exception).GetProperty("Message").GetGetMethod());
            generator.Emit(OpCodes.Callvirt, writeLineS);

            // Write StackTrace to Standard Output Error...
#if STACKTRACE
            generator.Emit(OpCodes.Call, standardErrorOutput);
            generator.Emit(OpCodes.Ldloc, exception);
            generator.Emit(OpCodes.Callvirt, typeof(Exception).GetProperty("StackTrace").GetGetMethod());
            generator.Emit(OpCodes.Callvirt, writeLineS);
#endif

            generator.EndScope();

            // Exit with code 1...
            generator.Emit(OpCodes.Ldc_I4_1);
            generator.Emit(OpCodes.Call, typeof(Environment).GetMethod("Exit"));

            generator.EndExceptionBlock();

            generator.Emit(OpCodes.Ret);

            // Make executable...
            programBuilder.CreateType();
            moduleBuilder.CreateGlobalFunctions();
            assembly.Save(filename);
        }

        #region Generation Standard Library

        public void chr(CodeGenerator cg)
        {
            RoutineInfo chrFunction = Scope.StandardFunctions["chr"];
            chrFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "chr",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(string),
                new Type[] { typeof(int) }
                );

            ILGenerator generator = (chrFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            Label exception = generator.DefineLabel();
            Label end = generator.DefineLabel();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldc_I4_0);
            generator.Emit(OpCodes.Blt, exception);

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldc_I4, 127);
            generator.Emit(OpCodes.Bgt, exception);
            generator.Emit(OpCodes.Br, end);

            // throw exception
            generator.MarkLabel(exception);
            generator.Emit(OpCodes.Newobj, typeof(ArgumentOutOfRangeException).GetConstructor(System.Type.EmptyTypes));
            generator.Emit(OpCodes.Throw);

            generator.MarkLabel(end);
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Conv_U2);
            generator.Emit(OpCodes.Box, typeof(char));
            generator.Emit(OpCodes.Callvirt, typeof(object).GetMethod("ToString", System.Type.EmptyTypes));
            generator.Emit(OpCodes.Ret);
        }

        public void concat(CodeGenerator cg)
        {
            Scope.StandardFunctions["concat"].GeneratedMethod = typeof(string).GetMethod("Concat", new Type[] { typeof(string), typeof(string) });
        }

        public void exit(CodeGenerator cg)
        {
            Scope.StandardFunctions["exit"].GeneratedMethod = typeof(Environment).GetMethod("Exit");
        }

        public void getline(CodeGenerator cg)
        {
            Scope.StandardFunctions["getline"].GeneratedMethod = typeof(Console).GetMethod("ReadLine");
        }

        public void not(CodeGenerator cg)
        {
            RoutineInfo notFunction = Scope.StandardFunctions["not"];
            notFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "not",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(int),
                new Type[] { typeof(int) }
                );

            ILGenerator generator = (notFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldc_I4_0);
            generator.Emit(OpCodes.Ceq);
            generator.Emit(OpCodes.Ret);
        }

        public void ord(CodeGenerator cg)
        {
            RoutineInfo ordFunction = Scope.StandardFunctions["ord"];
            ordFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "ord",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(int),
                new Type[] { typeof(string) }
                );

            ILGenerator generator = (ordFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            // Defining labels...
            Label emptyString = generator.DefineLabel();
            Label end = generator.DefineLabel();

            // Get length...
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Call, typeof(string).GetProperty("Length").GetGetMethod());
            generator.Emit(OpCodes.Ldc_I4_0);
            generator.Emit(OpCodes.Beq, emptyString);

            // Indexing...
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldc_I4_0);
            generator.Emit(OpCodes.Call, typeof(string).GetProperty("Chars").GetGetMethod());
            generator.Emit(OpCodes.Conv_I);
            generator.Emit(OpCodes.Br, end);

            // EmptyString => -1
            generator.MarkLabel(emptyString);
            generator.Emit(OpCodes.Ldc_I4_M1);

            generator.MarkLabel(end);
            generator.Emit(OpCodes.Ret);
        }

        public void print(CodeGenerator cg)
        {
            Scope.StandardFunctions["print"].GeneratedMethod = typeof(Console).GetMethod("Write", new Type[] { typeof(string) });
        }

        public void printi(CodeGenerator cg)
        {
            Scope.StandardFunctions["printi"].GeneratedMethod = typeof(Console).GetMethod("Write", new Type[] { typeof(int) });
        }

        public void printiline(CodeGenerator cg)
        {
            RoutineInfo printilineFunction = Scope.StandardFunctions["printiline"];
            printilineFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "printiline",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(void),
                new Type[] { typeof(int) }
                );

            ILGenerator generator = (printilineFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(int) }));
            generator.Emit(OpCodes.Ldstr, "\r\n");
            generator.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(string) }));
            generator.Emit(OpCodes.Ret);
        }

        public void printline(CodeGenerator cg)
        {
            RoutineInfo printlineFunction = Scope.StandardFunctions["printline"];
            printlineFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "printline",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(void),
                new Type[] { typeof(string) }
                );

            ILGenerator generator = (printlineFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(string) }));
            generator.Emit(OpCodes.Ldstr, "\r\n");
            generator.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(string) }));
            generator.Emit(OpCodes.Ret);
        }

        public void size(CodeGenerator cg)
        {
            RoutineInfo sizeFunction = Scope.StandardFunctions["size"];
            sizeFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "size",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(int),
                new Type[] { typeof(string) }
                );

            ILGenerator generator = (sizeFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Callvirt, typeof(string).GetProperty("Length").GetGetMethod());
            generator.Emit(OpCodes.Ret);
        }

        public void substring(CodeGenerator cg)
        {
            RoutineInfo substringFunction = Scope.StandardFunctions["substring"];
            substringFunction.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                "substring",
                MethodAttributes.Public | MethodAttributes.Static,
                typeof(string),
                new Type[] { typeof(string), typeof(int), typeof(int) }
                );

            ILGenerator generator = (substringFunction.GeneratedMethod as MethodBuilder).GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldarg_1);
            generator.Emit(OpCodes.Ldarg_2);
            generator.Emit(OpCodes.Callvirt, typeof(string).GetMethod("Substring", new Type[] { typeof(int), typeof(int) }));
            generator.Emit(OpCodes.Ret);
        }

        #endregion

        public void GenerateStandardLibrary(CodeGenerator cg)
        {
            chr(cg);
            concat(cg);
            exit(cg);
            getline(cg);
            not(cg);
            ord(cg);
            print(cg);
            printi(cg);
            printiline(cg);
            printline(cg);
            size(cg);
            substring(cg);
        }
    }
}
