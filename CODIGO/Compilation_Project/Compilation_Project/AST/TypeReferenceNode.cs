﻿using Antlr.Runtime;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class TypeReferenceNode : LanguageNode
    {
        #region Constructors

        public TypeReferenceNode() : base() { }

        public TypeReferenceNode(IToken token) : base(token) { }

        public TypeReferenceNode(TypeReferenceNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public string TypeName
        {
            get { return (Children[0] as IdNode).Text; }
        }

        public TigerType ReferencedType { get; set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking type...
            TigerType type;
            if (!scope.TryGetType(TypeName, out type))
            {
                _IsOk = false;
                ReferencedType = null;
                report.Add(SemanticInfo.UndefinedTypeReferenced(TypeName, this));
                return;
            }

            // Ok
            _IsOk = true;
            ReferencedType = type;
            ReferencedType.IsUsed = true;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // do nothing...
        }
    }

}
