﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class IfThenElseNode : ExpressionNode
    {
        #region Constructors

        public IfThenElseNode() : base() { }

        public IfThenElseNode(IToken token) : base(token) { }

        public IfThenElseNode(IfThenElseNode node) : base(node) { }

        #endregion

        public ExpressionNode Condition
        {
            get { return Children[0] as ExpressionNode; }
        }

        public ExpressionNode Then
        {
            get { return Children[1] as ExpressionNode; }
        }

        public ExpressionNode Else
        {
            get { return Children[2] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking childrens...
            Condition.CheckSemantics(scope, report);
            Then.CheckSemantics(scope, report);
            Else.CheckSemantics(scope, report);
            if (!Condition.IsOk || !Then.IsOk || !Else.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = Then.ExpressionType;

            // Type-checking condition...
            if (TigerType.Integer != Condition.ExpressionType)
            {
                report.Add(SemanticInfo.InvalidCondition(Condition.ExpressionType.Name, Condition));
                ExpressionType = TigerType.Error;
            }

            // Type-checking return...
            if (!TigerType.AreCompatibles(Then.ExpressionType, Else.ExpressionType))
            {
                report.Add(SemanticInfo.IncompatibleTypes(Then.ExpressionType.Name, Else.ExpressionType.Name, Else));
                ExpressionType = TigerType.Error;
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Defining labels...
            Label end = cg.Generator.DefineLabel();
            Label @else = cg.Generator.DefineLabel();

            // Generate & test condition...
            Condition.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Brfalse, @else);

            // Generate then body...
            Then.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Br, end);

            // Generate else body...
            cg.Generator.MarkLabel(@else);
            Else.GenerateCode(cg);

            cg.Generator.MarkLabel(end);
        }
    }

}
