﻿using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Compilation_Project.Parsing;

using System.Reflection;
using Compilation_Project.AST.BinaryOperators.ArithmeticOperators;
using Compilation_Project.AST.BinaryOperators.Logical_Operator;
using Compilation_Project.AST.BinaryOperators.ComparisonOperators;
using Compilation_Project.AST.BasicNodes;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.AST.LValue;
using Compilation_Project.AST.Statement;
using Compilation_Project.AST.Declaration;
using Compilation_Project.AST.Declaration.Sequence;
using Compilation_Project.AST.Declaration.Functions;
using Compilation_Project.AST.Declaration.Variable;
using Compilation_Project.AST.Declaration.Type;
using Compilation_Project.AST.Unary;

namespace Compilation_Project.AST
{
    public class Adaptor : CommonTreeAdaptor
    {
        public Adaptor() : base() { }

        public override object Create(IToken payload)
        {
            if (payload == null)
                return new NilNode(payload);

            switch (payload.Type)
            {
                case GrammarParser.PROGRAM_TREE:
                    return new ProgramNode(payload);
                case GrammarParser.PLUS:
                    return new PlusNode(payload);
                case GrammarParser.MINUS:
                    return new MinusNode(payload);
                case GrammarParser.MULTIPLICATION:
                    return new MultiplicationNode(payload);
                case GrammarParser.DIVIDE:
                    return new DivideNode(payload);
                case GrammarParser.OR:
                    return new OrNode(payload);
                case GrammarParser.AND:
                    return new AndNode(payload);
                case GrammarParser.EQUAL:
                    return new EqualNode(payload);
                case GrammarParser.NOT_EQUAL:
                    return new NotEqualNode(payload);
                case GrammarParser.GREATER_THAN:
                    return new GreaterThanNode(payload);
                case GrammarParser.LESS_THAN:
                    return new LessThanNode(payload);
                case GrammarParser.GT_EQUAL:
                    return new GtEqualNode(payload);
                case GrammarParser.LT_EQUAL:
                    return new LtEqualNode(payload);
                case GrammarParser.INT:
                    return new IntNode(payload);
                case GrammarParser.STRING:
                    return new StringNode(payload);
                case GrammarParser.NIL:
                    return new NilNode(payload);
                case GrammarParser.ID:
                    return new IdNode(payload);
                case GrammarParser.LVALUE_TREE:
                    return new ValueNode(payload);
                case GrammarParser.DOT_TREE:
                    return new DotNode(payload);
                case GrammarParser.INDEX_TREE:
                    return new IndexNode(payload);
                case GrammarParser.FOR_TREE:
                    return new ForNode(payload);
                case GrammarParser.WHILE_TREE:
                    return new WhileNode(payload);
                case GrammarParser.BREAK_TREE:
                    return new BreakNode(payload);
                case GrammarParser.ASSIGN:
                    return new AssignNode(payload);
                case GrammarParser.IF_THEN_ELSE_TREE:
                    return new IfThenElseNode(payload);
                case GrammarParser.IF_THEN_TREE:
                    return new IfThenNode(payload);
                case GrammarParser.ARRAY_TREE:
                    return new ArrayNode(payload);
                case GrammarParser.DEC_BLOCK_TREE:
                    return new LetInEndNode(payload);
                case GrammarParser.DECLARATION_LIST_TREE:
                    return new DeclarationListNode(payload);
                case GrammarParser.DEFINITION_FIELD_TREE:
                    return new FieldDefinitionNode(payload);
                case GrammarParser.DEFINITION_FIELDS_TREE:
                    return new FieldsDefinitionNode(payload);
                case GrammarParser.TYPE_REFERENCE_TREE:
                    return new TypeReferenceNode(payload);
                case GrammarParser.DECLARATION_FUNCTION_TREE:
                    return new DeclarationFunctionSeq(payload);
                case GrammarParser.EXPRESSION_SEQ_TREE:
                    return new ExpressionSequenceNode(payload);
                case GrammarParser.PROCEDURE_DECLARATION_TREE:
                    return new ProcedureDeclarationNode(payload);
                case GrammarParser.FUNCTION_DECLARATION_TREE:
                    return new FunctionDeclarationNode(payload);
                case GrammarParser.IMPLICIT_DECLARATION_TREE:
                    return new ImplicitVariableDeclarationNode(payload);
                case GrammarParser.EXPLICIT_DECLARATION_TREE:
                    return new ExplicitVariableDeclarationNode(payload);
                case GrammarParser.DECLARATION_VARIABLE_TREE:
                    return new DeclarationVariableSeq(payload);
                case GrammarParser.DECLARATION_TYPE_TREE:
                    return new DeclarationTypeSeq(payload);
                case GrammarParser.ALIAS_DECLARATION_TREE:
                    return new AliasDeclarationNode(payload);
                case GrammarParser.DECLARATION_ARRAY_TREE:
                    return new DeclarationArrayNode(payload);
                case GrammarParser.DECLARATION_RECORD_TREE:
                    return new DeclarationRecordNode(payload);
                case GrammarParser.NEGATIVE_TREE:
                    return new NegativeNode(payload);
                case GrammarParser.FUNCTION_CALL_TREE:
                    return new FunctionCallNode(payload);
                case GrammarParser.RECORD_CREATION_TREE:
                    return new RecordCreationNode(payload);
                case GrammarParser.FIELDS_CREATION_TREE:
                    return new FieldsCreationNode(payload);
                case GrammarParser.FIELD_CREATION_TREE:
                    return new FieldCreationNode(payload);
            }

            return new UnknownNode(payload);
        }
    }
}
