﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using Compilation_Project.CodeGeneration;


namespace Compilation_Project.AST
{
    public abstract class LValueNode : ExpressionNode
    {
        #region Constructors

        public LValueNode() : base() { }

        public LValueNode(IToken token) : base(token) { }

        public LValueNode(LValueNode node) : base(node) { }

        #endregion

        public abstract void Load(CodeGenerator cg);

        public abstract void Set(CodeGenerator cg);
    }
}
