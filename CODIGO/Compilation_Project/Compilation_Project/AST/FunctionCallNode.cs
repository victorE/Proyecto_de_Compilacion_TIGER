﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class FunctionCallNode : ExpressionNode
    {
        #region Constructors

        public FunctionCallNode() : base() { }

        public FunctionCallNode(IToken token) : base(token) { }

        public FunctionCallNode(FunctionCallNode node) : base(node) { }

        #endregion

        public IEnumerable<ExpressionNode> Arguments
        {
            get { return Children.Skip(1).Cast<ExpressionNode>(); }
        }

        public int ArgumentCount
        {
            get { return ChildCount - 1; }
        }

        public string FunctionName
        {
            get { return (Children[0] as IdNode).Name; }
        }

        public RoutineInfo ReferencedRoutine { get; private set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking function existence...
            RoutineInfo value;
            if (!scope.TryGetRoutine(FunctionName, out value))
            {
                report.Add(SemanticInfo.UndefinedFunctionReferenced(FunctionName, this));
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ReferencedRoutine = value;
            ReferencedRoutine.IsUsed = true;
            ExpressionType = ReferencedRoutine.ReturnType;

            // Checking number of parameters...
            if (value.ParameterCount != ArgumentCount)
            {
                report.Add(SemanticInfo.WrongParameterNumber(FunctionName, value.ParameterCount, ArgumentCount, this));
                ExpressionType = TigerType.Error;
                return;
            }

            int i = 0;
            foreach (var argument in Arguments)
            {
                argument.CheckSemantics(scope, report);
                var currParameter = ReferencedRoutine.Parameters[i++];

                // Checking childrens...
                if (!argument.IsOk)
                    ExpressionType = TigerType.Error;
                else if (!TigerType.AreCompatibles(currParameter.Type, argument.ExpressionType))
                {
                    // Type-checking arguments...
                    report.Add(SemanticInfo.IncompatibleTypes(currParameter.Type.Name, argument.ExpressionType.Name, argument));
                    ExpressionType = TigerType.Error;
                }
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            foreach (var item in Arguments)
                item.GenerateCode(cg);

            cg.Generator.Emit(OpCodes.Call, ReferencedRoutine.GeneratedMethod);
        }
    }

}
