﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Unary
{
    public class NegativeNode : UnaryExpressionNode
    {
        #region Constructors

        public NegativeNode() : base() { }

        public NegativeNode(IToken token) : base(token) { }

        public NegativeNode(NegativeNode node) : base(node) { }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking base...
            base.CheckSemantics(scope, report);
            if (ExpressionType == TigerType.Error)
                return;

            // Type-checking operand...
            if (Operand.ExpressionType != TigerType.Integer)
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.IncompatibleTypes(TigerType.Integer.Name, Operand.ExpressionType.Name, this));
                return;
            }

            // Ok
            ExpressionType = TigerType.Integer;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            Operand.GenerateCode(cg);

            cg.Generator.Emit(OpCodes.Neg);
        }
    }

}
