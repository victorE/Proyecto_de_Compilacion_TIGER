﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using Compilation_Project.Basic_Types;

namespace Compilation_Project.AST
{
    public abstract class ExpressionNode:LanguageNode
    {
        public ExpressionNode() : base() { }

        public ExpressionNode(IToken token) : base(token) { }

        public ExpressionNode(ExpressionNode node) : base(node) { }



        public override bool IsOk
        {
            get { return !(ExpressionType == null || ExpressionType == TigerType.Error); }
        }

        public TigerType ExpressionType { get; set; }
    }
}
