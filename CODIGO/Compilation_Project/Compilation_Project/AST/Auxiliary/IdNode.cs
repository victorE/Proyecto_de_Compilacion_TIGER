﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;

namespace Compilation_Project.AST.Auxiliary
{
    public class IdNode : AuxiliaryNode
    {
        #region Constructors

        public IdNode() : base() { }

        public IdNode(IToken token) : base(token) { }

        public IdNode(IdNode node) : base(node) { }

        #endregion

        public string Name
        {
            get { return this.Text; }
        }
    }
}
