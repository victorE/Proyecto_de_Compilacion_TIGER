﻿using Antlr.Runtime;

namespace Compilation_Project.AST
{
     class NullNode:AuxiliaryNode
    {
        public NullNode() : base() { }

        public NullNode(IToken token) : base(token) { }

        public NullNode(NullNode node) : base(node) { }
    }
}