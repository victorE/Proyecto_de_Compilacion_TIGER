﻿using Antlr.Runtime;

namespace Compilation_Project.AST
{
    public class UnknownNode:AuxiliaryNode
    {
            public UnknownNode() : base() { }

            public UnknownNode(IToken token) : base(token) { }

            public UnknownNode(AuxiliaryNode node) : base(node) { }
        
    }
}