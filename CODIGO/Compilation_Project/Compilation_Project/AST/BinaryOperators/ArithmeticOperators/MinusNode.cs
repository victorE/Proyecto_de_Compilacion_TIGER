﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ArithmeticOperators
{
    public class MinusNode : ArithmeticNode
    {
        #region Constructors

        public MinusNode() : base() { }

        public MinusNode(IToken token) : base(token) { }

        public MinusNode(MinusNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Sub; }
        }
    }
}
