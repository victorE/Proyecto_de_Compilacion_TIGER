﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ArithmeticOperators
{
    public class PlusNode : ArithmeticNode
    {
      
        public PlusNode() : base() { }

        public PlusNode(IToken token) : base(token) { }

        public PlusNode(PlusNode node) : base(node) { }


        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Add; }
        }
    }
}
