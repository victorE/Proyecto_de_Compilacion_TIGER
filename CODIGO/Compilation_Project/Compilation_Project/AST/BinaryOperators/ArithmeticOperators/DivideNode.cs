﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ArithmeticOperators
{
    public class DivideNode : ArithmeticNode
    {
        #region Constructors

        public DivideNode() : base() { }

        public DivideNode(IToken token) : base(token) { }

        public DivideNode(DivideNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Div; }
        }
    }
}
