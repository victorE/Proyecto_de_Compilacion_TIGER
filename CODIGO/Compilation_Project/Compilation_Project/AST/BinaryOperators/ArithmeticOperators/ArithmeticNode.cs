﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using Compilation_Project.Semantic;
using Compilation_Project.Basic_Types;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.AST.BinaryOperators.ArithmeticOperators
{
    public abstract class ArithmeticNode : BinaryExpressionNode
    {
       
        public ArithmeticNode() : base() { }

        public ArithmeticNode(IToken token) : base(token) { }

        public ArithmeticNode(ArithmeticNode node) : base(node) { }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking base...
            base.CheckSemantics(scope, report);
            if (ExpressionType == TigerType.Error)
                return;

            // si alguno de los dos operadores no son un numero entonces devolver el error
            //directamente como hacer eso
            if (LeftExpression.ExpressionType != TigerType.Integer || RightExpression.ExpressionType != TigerType.Integer)
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.InvalidOperation(Text, LeftExpression.ExpressionType.Name, RightExpression.ExpressionType.Name, this));
                return;
            }

            // Ok
            ExpressionType = TigerType.Integer;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Generate left exppresion...
            LeftExpression.GenerateCode(cg);

            // Generate right expression...
            RightExpression.GenerateCode(cg);

            // Make operation...
            cg.Generator.Emit(OperatorOpCode);
        }
    }
}
