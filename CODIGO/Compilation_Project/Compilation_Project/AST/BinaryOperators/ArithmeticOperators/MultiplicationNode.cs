﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;


namespace Compilation_Project.AST.BinaryOperators.ArithmeticOperators
{
    public class MultiplicationNode : ArithmeticNode
    {
        #region Constructors

        public MultiplicationNode() : base() { }

        public MultiplicationNode(IToken token) : base(token) { }

        public MultiplicationNode(MultiplicationNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Mul; }
        }
    }
    
}
