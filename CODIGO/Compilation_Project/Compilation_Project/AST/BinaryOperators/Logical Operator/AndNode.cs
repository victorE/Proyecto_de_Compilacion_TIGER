﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.Logical_Operator
{
    public class AndNode : LogicalNode
    {

        public AndNode() : base() { }

        public AndNode(IToken token) : base(token) { }

        public AndNode(AndNode node) : base(node) { }


        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Brfalse; }
        }
    }
}
