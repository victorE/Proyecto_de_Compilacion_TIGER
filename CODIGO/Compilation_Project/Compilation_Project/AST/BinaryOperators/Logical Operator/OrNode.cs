﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;


namespace Compilation_Project.AST.BinaryOperators.Logical_Operator
{
    public class OrNode : LogicalNode
    {
        #region Constructors

        public OrNode() : base() { }

        public OrNode(IToken token) : base(token) { }

        public OrNode(OrNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Brtrue; }
        }
    }
}
