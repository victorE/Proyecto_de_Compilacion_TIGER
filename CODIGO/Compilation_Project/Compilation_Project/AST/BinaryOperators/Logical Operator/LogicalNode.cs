﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.BinaryOperators.Logical_Operator
{
   public abstract class LogicalNode : BinaryExpressionNode
        {
            #region Constructors

            public LogicalNode() : base() { }

            public LogicalNode(IToken token) : base(token) { }

            public LogicalNode(LogicalNode node) : base(node) { }

            #endregion

            public override void CheckSemantics(Scope scope, Report report)
            {
                // Checking base...
                base.CheckSemantics(scope, report);
                if (ExpressionType == TigerType.Error)
                    return;

                    // si alguno de los dos operadores no son un numero entonces devolver el error
                     //directamente como hacer eso
                if (LeftExpression.ExpressionType != TigerType.Integer || RightExpression.ExpressionType != TigerType.Integer)
                {
                    ExpressionType = TigerType.Error;
                    report.Add(SemanticInfo.InvalidOperation(Text, LeftExpression.ExpressionType.Name, RightExpression.ExpressionType.Name, this));
                    return;
                }

                // Ok
                ExpressionType = TigerType.Integer;
            }

            public override void GenerateCode(CodeGenerator cg)
            {
                // Defining labels...
                Label end = cg.Generator.DefineLabel();

                // Generate left expression...
                LeftExpression.GenerateCode(cg);

                // Duplicate hypothetical result...
                cg.Generator.Emit(OpCodes.Dup);

                // Test left operand...
                cg.Generator.Emit(OperatorOpCode, end);

                // Remove hypothetical result...
                cg.Generator.Emit(OpCodes.Pop);

                // Generate right operand as result...
                RightExpression.GenerateCode(cg);

                cg.Generator.MarkLabel(end);

                // Set 1 or 0...
                cg.Generator.Emit(OpCodes.Ldc_I4_0);
                cg.Generator.Emit(OpCodes.Ceq);
                cg.Generator.Emit(OpCodes.Ldc_I4_1);
                cg.Generator.Emit(OpCodes.Xor);
            }
        }
}
