﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public class LessThanNode : OrderNode
    {
        #region Constructors

        public LessThanNode() : base() { }

        public LessThanNode(IToken token) : base(token) { }

        public LessThanNode(LessThanNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Blt; }
        }
    }
}
