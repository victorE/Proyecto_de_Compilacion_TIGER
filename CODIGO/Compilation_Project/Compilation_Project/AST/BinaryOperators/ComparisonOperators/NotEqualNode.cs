﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public class NotEqualNode : IdentityNode
    {

        public NotEqualNode() : base() { }

        public NotEqualNode(IToken token) : base(token) { }

        public NotEqualNode(NotEqualNode node) : base(node) { }


        public override OpCode OperatorOpCode
        {
            get { throw new NotImplementedException(); }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            base.GenerateCode(cg);

            // Make operation...
            cg.Generator.Emit(OpCodes.Ceq);
            cg.Generator.Emit(OpCodes.Ldc_I4_1);
            cg.Generator.Emit(OpCodes.Xor);
        }
    }

}
