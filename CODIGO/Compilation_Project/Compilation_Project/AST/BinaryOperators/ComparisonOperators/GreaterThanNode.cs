﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public class GreaterThanNode : OrderNode
    {
        #region Constructors

        public GreaterThanNode() : base() { }

        public GreaterThanNode(IToken token) : base(token) { }

        public GreaterThanNode(GreaterThanNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Bgt; }
        }
    }
}
