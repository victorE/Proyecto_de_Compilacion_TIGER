﻿using System;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public abstract class IdentityNode : ComparisonNode
    {
     

        public IdentityNode() : base() { }

        public IdentityNode(IToken token) : base(token) { }

        public IdentityNode(IdentityNode node) : base(node) { }

     

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking base...
            base.CheckSemantics(scope, report);
            if (ExpressionType == TigerType.Error)
                return;

            // Type-checking operands...
            if (!TigerType.AreCompatibles(LeftExpression.ExpressionType, RightExpression.ExpressionType) ||
                (LeftExpression.ExpressionType == TigerType.Nil && RightExpression.ExpressionType == TigerType.Nil))
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.InvalidOperation(Text, LeftExpression.ExpressionType.Name, RightExpression.ExpressionType.Name, this));
                return;
            }

            // Ok
            ExpressionType = TigerType.Integer;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Generate left operand...
            LeftExpression.GenerateCode(cg);

            // Generate right operand...
            RightExpression.GenerateCode(cg);

            // Test case of string...
            if (LeftExpression.ExpressionType == TigerType.String || RightExpression.ExpressionType == TigerType.String)
            {
                cg.Generator.Emit(OpCodes.Call, typeof(string).GetMethod("Compare", new Type[] { typeof(string), typeof(string) }));
                cg.Generator.Emit(OpCodes.Ldc_I4_0);
            }
        }
    }

}