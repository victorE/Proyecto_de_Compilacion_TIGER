﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public class LtEqualNode : OrderNode
    {
        
        public LtEqualNode() : base() { }

        public LtEqualNode(IToken token) : base(token) { }

        public LtEqualNode(LtEqualNode node) : base(node) { }


        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Ble; }
        }
    }
}
