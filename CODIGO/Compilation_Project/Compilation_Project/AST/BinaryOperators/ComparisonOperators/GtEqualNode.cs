﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public class GtEqualNode : OrderNode
    {
       
        public GtEqualNode() : base() { }

        public GtEqualNode(IToken token) : base(token) { }

        public GtEqualNode(GtEqualNode node) : base(node) { }


        public override OpCode OperatorOpCode
        {
            get { return OpCodes.Bge; }
        }
    }
   
}
