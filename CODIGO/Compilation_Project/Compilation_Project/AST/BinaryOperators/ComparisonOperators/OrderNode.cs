﻿using System;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;


namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public abstract class OrderNode : ComparisonNode
    {
        #region Constructors

        public OrderNode() : base() { }

        public OrderNode(IToken token) : base(token) { }

        public OrderNode(OrderNode node) : base(node) { }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking base...
            base.CheckSemantics(scope, report);
            if (ExpressionType == TigerType.Error)
                return;

            // Type-checking expression...
            if ((LeftExpression.ExpressionType == TigerType.Integer && RightExpression.ExpressionType == TigerType.Integer) ||
                (TigerType.AreCompatibles(LeftExpression.ExpressionType, TigerType.String) && TigerType.AreCompatibles(RightExpression.ExpressionType, TigerType.String) &&
                (LeftExpression.ExpressionType != TigerType.Nil || RightExpression.ExpressionType != TigerType.Nil)))
            {
                // Ok
                ExpressionType = TigerType.Integer;
                return;
            }

            // Error
            ExpressionType = TigerType.Error;
            report.Add(SemanticInfo.InvalidOperation(Text, LeftExpression.ExpressionType.Name, RightExpression.ExpressionType.Name, this));
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            Label success = cg.Generator.DefineLabel();
            Label end = cg.Generator.DefineLabel();

            // Generate left operand...
            LeftExpression.GenerateCode(cg);

            // Generate right operand...
            RightExpression.GenerateCode(cg);

            // Test case of string...
            if (LeftExpression.ExpressionType == TigerType.String || RightExpression.ExpressionType == TigerType.String)
            {
                cg.Generator.Emit(OpCodes.Call, typeof(string).GetMethod("Compare", new Type[] { typeof(string), typeof(string) }));
                cg.Generator.Emit(OpCodes.Ldc_I4_0);
            }

            // Jump if success...
            cg.Generator.Emit(OperatorOpCode, success);

            // fail...
            cg.Generator.Emit(OpCodes.Ldc_I4_0);
            cg.Generator.Emit(OpCodes.Br, end);

            // success..
            cg.Generator.MarkLabel(success);
            cg.Generator.Emit(OpCodes.Ldc_I4_1);

            cg.Generator.MarkLabel(end);
        }
    }

}
