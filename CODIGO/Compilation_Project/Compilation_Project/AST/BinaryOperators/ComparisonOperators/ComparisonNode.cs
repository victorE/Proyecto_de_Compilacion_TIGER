﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public abstract class ComparisonNode : BinaryExpressionNode
    {
        #region Constructors

        public ComparisonNode() : base() { }

        public ComparisonNode(IToken token) : base(token) { }

        public ComparisonNode(ComparisonNode node) : base(node) { }

        #endregion
    }
}
