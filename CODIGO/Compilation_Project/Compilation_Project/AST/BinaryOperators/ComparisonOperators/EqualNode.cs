﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;

namespace Compilation_Project.AST.BinaryOperators.ComparisonOperators
{
    public class EqualNode : IdentityNode
    {
        #region Constructors

        public EqualNode() : base() { }

        public EqualNode(IToken token) : base(token) { }

        public EqualNode(EqualNode node) : base(node) { }

        #endregion

        public override OpCode OperatorOpCode
        {
            get { throw new NotImplementedException(); }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            base.GenerateCode(cg);

            // Make operation...
            cg.Generator.Emit(OpCodes.Ceq);
        }
    }

}
