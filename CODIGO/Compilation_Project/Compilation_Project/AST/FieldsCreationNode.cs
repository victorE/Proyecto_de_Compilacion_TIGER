﻿using Antlr.Runtime;
using System.Collections.Generic;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;
using Compilation_Project.Utils;

namespace Compilation_Project.AST
{
    public class FieldsCreationNode : LanguageNode, IEnumerable<FieldCreationNode>
    {
        #region Constructors

        public FieldsCreationNode() : base() { }

        public FieldsCreationNode(IToken token) : base(token) { }

        public FieldsCreationNode(FieldsCreationNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public IEnumerable<FieldCreationNode> Fields
        {
            get { return Children.As<FieldCreationNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<FieldCreationNode> GetEnumerator()
        {
            return Fields.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Fields.GetEnumerator();
        }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Ok
            _IsOk = true;

            // Checking duplicated name...
            SortedSet<string> names = new SortedSet<string>();
            foreach (var field in Fields)
            {
                field.CheckSemantics(scope, report);
                if (!field.IsOk)
                {
                    _IsOk = false;
                    continue;
                }

                if (!names.Add(field.FieldName))
                {
                    _IsOk = false;
                    report.Add(SemanticInfo.DuplicatedField(field.FieldName, field));
                }
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // do nothing...
        }
    }
}
