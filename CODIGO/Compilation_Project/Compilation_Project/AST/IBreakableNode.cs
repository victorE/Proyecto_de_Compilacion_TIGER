﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection.Emit;

namespace Compilation_Project.AST
{
    public interface IBreakableNode
    {
        Label EndLoop { get; }
    }
}
