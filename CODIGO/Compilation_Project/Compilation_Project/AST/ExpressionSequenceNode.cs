﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;
using Compilation_Project.Utils;

namespace Compilation_Project.AST
{
    public class ExpressionSequenceNode : ExpressionNode, IEnumerable<ExpressionNode>
    {
        #region Constructors

        public ExpressionSequenceNode() : base() { }

        public ExpressionSequenceNode(IToken token) : base(token) { }

        public ExpressionSequenceNode(ExpressionSequenceNode node) : base(node) { }

        #endregion

        public IEnumerable<ExpressionNode> Expressions
        {
            get { return Children.As<ExpressionNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<ExpressionNode> GetEnumerator()
        {
            return Expressions.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Expressions.GetEnumerator();
        }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Set to null type...
            ExpressionType = null;

            // Checking childrens...
            foreach (var expression in Expressions)
                expression.CheckSemantics(scope, report);

            if (Expressions.Any(expression => !expression.IsOk))
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Patch by Clarification 8...
            if (ChildCount == 0 || ExpressionType != null)
            {
                ExpressionType = TigerType.Void;
                return;
            }

            // Ok
            ExpressionType = (Children[ChildCount - 1] as ExpressionNode).ExpressionType;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            TigerType type = TigerType.Void;
            foreach (var expression in Expressions)
            {
                if (type != TigerType.Void)
                    cg.Generator.Emit(OpCodes.Pop);

                type = expression.ExpressionType;
                expression.GenerateCode(cg);
            }

            if (type != TigerType.Void && ExpressionType == TigerType.Void)
                cg.Generator.Emit(OpCodes.Pop);
        }
    }

}
