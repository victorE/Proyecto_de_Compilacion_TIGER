﻿using Antlr.Runtime;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Variable
{
    public class ExplicitVariableDeclarationNode : VariableDeclarationNode
    {
        #region Constructors

        public ExplicitVariableDeclarationNode() : base() { }

        public ExplicitVariableDeclarationNode(IToken token) : base(token) { }

        public ExplicitVariableDeclarationNode(ExplicitVariableDeclarationNode node) : base(node) { }

        #endregion

        public TypeReferenceNode VariableType
        {
            get { return Children[2] as TypeReferenceNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking value before declaring...
            base.CheckSemantics(scope, report);

            // Checking type variable...
            VariableType.CheckSemantics(scope, report);
            if (!VariableType.IsOk)
            {
                _IsOk = false;
                return;
            }

            // Checking duplicated variable...
            if (scope.ExistVariableInCurrentContext(Name))
            {
                _IsOk = false;
                report.Add(SemanticInfo.DuplicatedVariable(Name, this));
                return;
            }

            // Declaring...
            DeclaredVariable = new VariableInfo()
            {
                IsParameter = false,
                IsReadOnly = false,
                Name = Name,
                Type = VariableType.ReferencedType
            };
            scope.DefineVariable(DeclaredVariable);

            // Checking children...
            if (!IsOk)
                return;

            // Type-checking expression...
            if (!TigerType.AreCompatibles(DeclaredVariable.Type, Value.ExpressionType))
            {
                _IsOk = false;
                report.Add(SemanticInfo.IncompatibleTypes(DeclaredVariable.Type.Name, Value.ExpressionType.Name, Value));
                return;
            }

            // Ok
            _IsOk = true;
        }
    }
}
