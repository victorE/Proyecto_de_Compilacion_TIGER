﻿using Antlr.Runtime;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Variable
{
    public class ImplicitVariableDeclarationNode : VariableDeclarationNode
    {
        #region Constructors

        public ImplicitVariableDeclarationNode() : base() { }

        public ImplicitVariableDeclarationNode(IToken token) : base(token) { }

        public ImplicitVariableDeclarationNode(ImplicitVariableDeclarationNode node) : base(node) { }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking value before declaring...
            base.CheckSemantics(scope, report);

            // Checking duplicated variable...
            if (scope.ExistVariableInCurrentContext(Name))
            {
                _IsOk = false;
                report.Add(SemanticInfo.DuplicatedVariable(Name, this));
                return;
            }

            // Checking children...
            if (!_IsOk)
                return;

            // Type-checking inferred type...
            if (Value.ExpressionType == TigerType.Void ||
                Value.ExpressionType == TigerType.Nil)
            {
                _IsOk = false;
                report.Add(SemanticInfo.InvalidInferredType(Name, this));
                return;
            }

            // Declaring...
            DeclaredVariable = new VariableInfo()
            {
                IsParameter = false,
                IsReadOnly = false,
                Name = Name,
                Type = Value.ExpressionType
            };
            scope.DefineVariable(DeclaredVariable);

            // Ok
            _IsOk = true;
        }
    }

}
