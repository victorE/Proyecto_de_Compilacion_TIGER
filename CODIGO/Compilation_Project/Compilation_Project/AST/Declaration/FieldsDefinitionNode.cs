﻿using Antlr.Runtime;
using System.Collections.Generic;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;
using Compilation_Project.Utils;

namespace Compilation_Project.AST.Declaration
{
    public class FieldsDefinitionNode : LanguageNode, IEnumerable<FieldDefinitionNode>
    {
        #region Constructors

        public FieldsDefinitionNode() : base() { }

        public FieldsDefinitionNode(IToken token) : base(token) { }

        public FieldsDefinitionNode(FieldsDefinitionNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public IEnumerable<FieldDefinitionNode> TypeFields
        {
            get { return Children.As<FieldDefinitionNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<FieldDefinitionNode> GetEnumerator()
        {
            return TypeFields.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return TypeFields.GetEnumerator();
        }

        #endregion

        public List<FieldDefinitionNode> ValidTypeFields { get; private set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Ok
            _IsOk = true;
            ValidTypeFields = new List<FieldDefinitionNode>();

            // Checking duplicated name...
            SortedSet<string> names = new SortedSet<string>();
            foreach (var field in TypeFields)
            {
                field.CheckSemantics(scope, report);
                if (!field.IsOk)
                {
                    _IsOk = false;
                    continue;
                }

                if (!names.Add(field.FieldName))
                {
                    _IsOk = false;
                    report.Add(SemanticInfo.DuplicatedField(field.FieldName, field));
                    continue;
                }

                ValidTypeFields.Add(field);
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // do nothing
        }
    }

}
