﻿using Antlr.Runtime;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Functions
{
    public class ProcedureDeclarationNode : DeclarationFunctionNode
    {
        #region Constructors

        public ProcedureDeclarationNode() : base() { }

        public ProcedureDeclarationNode(IToken token) : base(token) { }

        public ProcedureDeclarationNode(ProcedureDeclarationNode node) : base(node) { }

        #endregion

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Checking base...
            base.CheckDeclaration(scope, report);
            if (!IsOk)
                return;

            // Set return type...
            _IsOk = true;
            DeclaredRoutine.ReturnType = TigerType.Void;
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking base...
            base.CheckSemantics(scope, report);
            if (!IsOk)
                return;

            // Type-checking return...
            if (!TigerType.AreCompatibles(TigerType.Void, Body.ExpressionType))
            {
                _IsOk = false;
                report.Add(SemanticInfo.IncompatibleTypes(TigerType.Void.Name, Body.ExpressionType.Name, Body));
                return;
            }

            // Ok
            _IsOk = true;
        }
    }

}
