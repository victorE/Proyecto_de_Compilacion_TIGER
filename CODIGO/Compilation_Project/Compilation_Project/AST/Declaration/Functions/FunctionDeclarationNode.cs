﻿using Antlr.Runtime;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Functions
{
    public class FunctionDeclarationNode : DeclarationFunctionNode
    {
        #region Constructors

        public FunctionDeclarationNode() : base() { }

        public FunctionDeclarationNode(IToken token) : base(token) { }

        public FunctionDeclarationNode(FunctionDeclarationNode node) : base(node) { }

        #endregion

        public TypeReferenceNode ReturnType
        {
            get { return Children[3] as TypeReferenceNode; }
        }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            base.CheckDeclaration(scope, report);

            // Checking return type & base...
            ReturnType.CheckSemantics(scope, report);
            if (!ReturnType.IsOk || !IsOk)
            {
                _IsOk = false;
                return;
            }

            // Set return type...
            _IsOk = true;
            DeclaredRoutine.ReturnType = ReturnType.ReferencedType;
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking base...
            base.CheckSemantics(scope, report);
            if (!IsOk)
                return;

            // Type-checking return...
            if (!TigerType.AreCompatibles(ReturnType.ReferencedType, Body.ExpressionType))
            {
                _IsOk = false;
                report.Add(SemanticInfo.IncompatibleTypes(ReturnType.ReferencedType.Name, Body.ExpressionType.Name, Body));
                return;
            }

            // Ok
            _IsOk = true;
        }
    }

}
