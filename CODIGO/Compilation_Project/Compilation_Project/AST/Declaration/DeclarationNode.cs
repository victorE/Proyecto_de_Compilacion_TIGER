﻿using Antlr.Runtime;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration
{
    public abstract class DeclarationNode : LanguageNode
    {
        #region Constructors

        public DeclarationNode() : base() { }

        public DeclarationNode(IToken token) : base(token) { }

        public DeclarationNode(DeclarationNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public abstract void CheckDeclaration(Scope scope, Report report);

        public abstract void GenerateDeclaration(CodeGenerator cg);
    }

}
