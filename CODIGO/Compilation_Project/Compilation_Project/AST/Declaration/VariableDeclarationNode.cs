﻿using Antlr.Runtime;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration
{
    public abstract class VariableDeclarationNode : DeclarationNode
    {
        #region Constructors

        public VariableDeclarationNode() : base() { }

        public VariableDeclarationNode(IToken token) : base(token) { }

        public VariableDeclarationNode(VariableDeclarationNode node) : base(node) { }

        #endregion

        public VariableInfo DeclaredVariable { get; protected set; }

        public string Name
        {
            get { return (Children[0] as IdNode).Text; }
        }

        public ExpressionNode Value
        {
            get { return Children[1] as ExpressionNode; }
        }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Dummy declaration...
            _IsOk = true;
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking children & declaration...
            Value.CheckSemantics(scope, report);
            if (!Value.IsOk || !IsOk)
            {
                _IsOk = false;
                return;
            }

            // Ok
            _IsOk = true;
        }

        public override void GenerateDeclaration(CodeGenerator cg)
        {
            // Define variable
            DeclaredVariable.CreateField(cg);
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Generate code for initial value
            Value.GenerateCode(cg);

            // Assign
            DeclaredVariable.SetValueFromStack(cg);
        }
    }

}
