﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using Compilation_Project.Utils;

namespace Compilation_Project.AST.Declaration.Sequence
{
    public class DeclarationVariableSeq : DeclarationSequenceNode, IEnumerable<VariableDeclarationNode>
    {
        #region Constructors

        public DeclarationVariableSeq() : base() { }

        public DeclarationVariableSeq(IToken token) : base(token) { }

        public DeclarationVariableSeq(DeclarationVariableSeq node) : base(node) { }

        #endregion

        public override IEnumerable<DeclarationNode> Declarations
        {
            get { return Children.As<VariableDeclarationNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<VariableDeclarationNode> GetEnumerator()
        {
            return Children.Cast<VariableDeclarationNode>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Children.Cast<VariableDeclarationNode>().GetEnumerator();
        }

        #endregion
    }

}
