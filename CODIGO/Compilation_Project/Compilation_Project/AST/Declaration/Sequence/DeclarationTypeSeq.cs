﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using Compilation_Project.AST.Declaration.Type;
using Compilation_Project.Basic_Types;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;
using Compilation_Project.Utils;


namespace Compilation_Project.AST.Declaration.Sequence
{
    public class DeclarationTypeSeq : DeclarationSequenceNode, IEnumerable<TypeDeclarationNode>
    {
        #region Constructors

        public DeclarationTypeSeq() : base() { }

        public DeclarationTypeSeq(IToken token) : base(token) { }

        public DeclarationTypeSeq(DeclarationTypeSeq node) : base(node) { }

        #endregion

        public override IEnumerable<DeclarationNode> Declarations
        {
            get { return Children.As<TypeDeclarationNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<TypeDeclarationNode> GetEnumerator()
        {
            return Children.Cast<TypeDeclarationNode>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Children.Cast<TypeDeclarationNode>().GetEnumerator();
        }

        #endregion

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Check base...
            base.CheckSemantics(scope, report);

            SortedDictionary<string, State> states = new SortedDictionary<string, State>();
            foreach (var declaration in Children.Cast<TypeDeclarationNode>())
            {
                switch (DagSearch(states, declaration.DeclaredType))
                {
                    case State.Cyclic:
                        report.Add(SemanticInfo.TypeCircularDependency(declaration.Name, declaration));
                        scope.RemoveInconsistentType(declaration.Name);

                        declaration.NotifyError();
                        break;
                    case State.NotFound:
                        scope.RemoveInconsistentType(declaration.Name);
                        declaration.NotifyError();
                        break;
                    default:
                        if (!(declaration is DeclarationRecordNode))
                            declaration.RectifyType(scope, report);
                        break;
                }
            }

            // Checking well defined types...
            foreach (var declaration in Children.OfType<DeclarationRecordNode>())
                declaration.RectifyType(scope, report);

            _IsOk = Declarations.All(node => node.IsOk);
        }

        private enum State { Ok, Cyclic, NotFound }

        private State DagSearch(SortedDictionary<string, State> states, TigerType node)
        {
            if (node == null)
                return State.NotFound;

            if (states.ContainsKey(node.Name))
                return states[node.Name];

            if (node is ArrayType)
            {
                ArrayType array = node as ArrayType;
                states.Add(node.Name, State.Cyclic);
                State result = states[node.Name] = DagSearch(states, array.ElementsType);

                if (result == State.Ok && array.ElementsType is AliasType)
                    array.ElementsType = (array.ElementsType as AliasType).BaseType;

                return result;
            }

            if (node is AliasType)
            {
                AliasType alias = node as AliasType;
                states.Add(node.Name, State.Cyclic);
                State result = states[node.Name] = DagSearch(states, alias.BaseType);

                if (result == State.Ok && alias.BaseType is AliasType)
                    alias.BaseType = (alias.BaseType as AliasType).BaseType;

                return result;
            }

            states.Add(node.Name, State.Ok);
            return State.Ok;
        }
    }

}
