﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using Compilation_Project.Utils;

namespace Compilation_Project.AST.Declaration.Sequence
{
    public class DeclarationFunctionSeq : DeclarationSequenceNode, IEnumerable<DeclarationFunctionNode>
    {
        #region Constructors

        public DeclarationFunctionSeq() : base() { }

        public DeclarationFunctionSeq(IToken token) : base(token) { }

        public DeclarationFunctionSeq(DeclarationFunctionSeq node) : base(node) { }

        #endregion

        public override IEnumerable<DeclarationNode> Declarations
        {
            get { return Children.As<DeclarationFunctionNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<DeclarationFunctionNode> GetEnumerator()
        {
            return Children.Cast<DeclarationFunctionNode>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Children.Cast<DeclarationFunctionNode>().GetEnumerator();
        }

        #endregion
    }

}
