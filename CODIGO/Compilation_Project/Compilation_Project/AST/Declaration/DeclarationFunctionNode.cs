﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration
{
    public abstract class DeclarationFunctionNode : DeclarationNode
    {
        #region Constructors

        public DeclarationFunctionNode() : base() { }

        public DeclarationFunctionNode(IToken token) : base(token) { }

        public DeclarationFunctionNode(DeclarationFunctionNode node) : base(node) { }

        #endregion

        #region Instances

        private List<VariableInfo> _InnerVariables;

        #endregion

        public ExpressionNode Body
        {
            get { return Children[1] as ExpressionNode; }
        }

        public RoutineInfo DeclaredRoutine { get; protected set; }

        public string Name
        {
            get { return (Children[0] as IdNode).Text; }
        }

        public FieldsDefinitionNode Parameters
        {
            get { return Children[2] as FieldsDefinitionNode; }
        }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Checking duplicated routine...
            if (scope.ExistRoutineInCurrentContext(Name))
            {
                _IsOk = false;
                report.Add(SemanticInfo.DuplicatedRoutine(Name, this));
                return;
            }

            // Checking parameters declarations...
            Parameters.CheckSemantics(scope, report);
            if (!Parameters.IsOk)
            {
                _IsOk = false;
                return;
            }

            // Ok
            _IsOk = true;
            DeclaredRoutine = new RoutineInfo()
            {
                IsStandard = false,
                Name = Name,
                Parameters = Parameters.TypeFields.Select(param => param.FieldInfo).ToArray()
            };
            scope.DefineRoutine(DeclaredRoutine);
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Verifying declaration...
            if (!IsOk)
                return;

            var bodyScope = scope.CreateChildScope("FUNCTION", false);
            foreach (var parameter in DeclaredRoutine.Parameters)
                bodyScope.DefineVariable(parameter);

            // Checking body & declaration error...
            Body.CheckSemantics(bodyScope, report);
            if (!Body.IsOk)
            {
                _IsOk = false;
                return;
            }

            // Ok
            _IsOk = true;
            _InnerVariables = new List<VariableInfo>(bodyScope.GetInnerVariables());
        }

        public override void GenerateDeclaration(CodeGenerator cg)
        {
            // Unused function
            if (!DeclaredRoutine.IsUsed)
                return;

            // Generate function...
            DeclaredRoutine.GeneratedMethod = cg.ClassBuilder.DefineMethod(
                DeclaredRoutine.FullName,
                MethodAttributes.Public | MethodAttributes.Static,
                DeclaredRoutine.ReturnType.GetCILType(cg.ModuleBuilder),
                DeclaredRoutine.Parameters.Select(item => item.Type.GetCILType(cg.ModuleBuilder)).ToArray()
                );

            foreach (var param in DeclaredRoutine.Parameters)
                param.CreateField(cg);
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Unused function
            if (!DeclaredRoutine.IsUsed)
                return;

            CodeGenerator cgBody = new CodeGenerator()
            {
                ClassBuilder = cg.ClassBuilder,
                Generator = (DeclaredRoutine.GeneratedMethod as MethodBuilder).GetILGenerator(),
                ModuleBuilder = cg.ModuleBuilder
            };

            cgBody.Generator.BeginScope();

            // Saving value variables...
            LocalBuilder[] backups = new LocalBuilder[_InnerVariables.Count];

            for (int i = 0; i < backups.Length; i++)
            {
                VariableInfo variable = _InnerVariables[i];
                variable.CreateField(cgBody);
                variable.LoadValueToStack(cgBody);

                backups[i] = cgBody.Generator.DeclareLocal(variable.Type.GetCILType(cg.ModuleBuilder));
                cgBody.Generator.Emit(OpCodes.Stloc, backups[i]);
            }

            // Set Parameters...
            for (int i = 0; i < DeclaredRoutine.ParameterCount; i++)
            {
                cgBody.Generator.Emit(OpCodes.Ldarg, i);
                cgBody.Generator.Emit(OpCodes.Stsfld, DeclaredRoutine.Parameters[i].GeneratedVariable);
            }

            // Generate Body...
            Body.GenerateCode(cgBody);

            // Restore values variables...
            for (int i = 0; i < backups.Length; i++)
            {
                cgBody.Generator.Emit(OpCodes.Ldloc, backups[i]);
                _InnerVariables[i].SetValueFromStack(cgBody);
            }

            cgBody.Generator.EndScope();

            cgBody.Generator.Emit(OpCodes.Ret);
        }
    }

}
