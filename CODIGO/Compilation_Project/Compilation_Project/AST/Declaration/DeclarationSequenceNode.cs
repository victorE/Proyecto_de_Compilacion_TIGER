﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration
{
    public abstract class DeclarationSequenceNode : LanguageNode
    {
        #region Constructors

        public DeclarationSequenceNode() : base() { }

        public DeclarationSequenceNode(IToken token) : base(token) { }

        public DeclarationSequenceNode(DeclarationSequenceNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public abstract IEnumerable<DeclarationNode> Declarations { get; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Declaring...
            foreach (var declaration in Declarations)
                declaration.CheckDeclaration(scope, report);

            // Check semantics...
            foreach (var declaration in Declarations)
                declaration.CheckSemantics(scope, report);

            // Checking all ok...
            _IsOk = Declarations.All(declaration => declaration.IsOk);
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // Generate declaration...
            foreach (var declaration in Declarations)
                declaration.GenerateDeclaration(cg);

            // Generate body...
            foreach (var declaration in Declarations)
                declaration.GenerateCode(cg);
        }
    }

}
