﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;
using Compilation_Project.Utils;

namespace Compilation_Project.AST.Declaration
{
    public class DeclarationListNode : LanguageNode, IEnumerable<DeclarationSequenceNode>
    {
        #region Constructors

        public DeclarationListNode() : base() { }

        public DeclarationListNode(IToken token) : base(token) { }

        public DeclarationListNode(DeclarationListNode node) : base(node) { }

        #endregion

        public IEnumerable<DeclarationSequenceNode> Sequences
        {
            get { return Children.As<DeclarationSequenceNode>(); }
        }

        #region IEnumerable Members

        public IEnumerator<DeclarationSequenceNode> GetEnumerator()
        {
            return Sequences.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Sequences.GetEnumerator();
        }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            foreach (var sequence in Sequences)
                sequence.CheckSemantics(scope, report);

            // Checking all ok...
            _IsOk = Sequences.All(sequence => sequence.IsOk);
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            foreach (var sequence in Sequences)
                sequence.GenerateCode(cg);
        }
    }

}
