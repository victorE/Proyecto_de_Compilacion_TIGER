﻿using Antlr.Runtime;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Type
{
    public class AliasDeclarationNode : TypeDeclarationNode
    {
        #region Constructors

        public AliasDeclarationNode() : base() { }

        public AliasDeclarationNode(IToken token) : base(token) { }

        public AliasDeclarationNode(AliasDeclarationNode node) : base(node) { }

        #endregion

        public TypeReferenceNode BaseType
        {
            get { return Children[1] as TypeReferenceNode; }
        }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Checking base...
            base.CheckDeclaration(scope, report);
            if (!IsOk)
                return;

            DeclaredType = new AliasType(Name);
            scope.DefineType(DeclaredType);
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            BaseType.CheckSemantics(scope, report);

            // Verifying declaration...
            if (!IsOk)
                return;

            // Checking children & declaration error...
            _IsOk = BaseType.IsOk;
            (DeclaredType as AliasType).BaseType = BaseType.ReferencedType;
        }

        public override void RectifyType(Scope scope, Report report)
        {
            // Verifying semantics...
            if (!IsOk)
                return;

            // Ok
            _IsOk = true;
            scope.ResolveType(Name, (DeclaredType as AliasType).BaseType);
        }

        public override void GenerateDeclaration(CodeGenerator cg)
        {
            // pass
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // pass
        }
    }

}
