﻿using Antlr.Runtime;
using System.Collections.Generic;
using System.Linq;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Type
{
    public class DeclarationRecordNode : TypeDeclarationNode
    {
        #region Constructors

        public DeclarationRecordNode() : base() { }

        public DeclarationRecordNode(IToken token) : base(token) { }

        public DeclarationRecordNode(DeclarationRecordNode node) : base(node) { }

        #endregion

        public FieldsDefinitionNode RecordFields
        {
            get { return Children[1] as FieldsDefinitionNode; }
        }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Checking base...
            base.CheckDeclaration(scope, report);
            if (!IsOk)
                return;

            // Ok
            _IsOk = true;
            DeclaredType = new RecordType(Name);
            scope.DefineType(DeclaredType);
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // pass
        }

        public override void RectifyType(Scope scope, Report report)
        {
            RecordFields.CheckSemantics(scope, report);

            // Verifying declaration...
            if (!IsOk)
                return;

            // Ok
            _IsOk = RecordFields.IsOk;
            (DeclaredType as RecordType).Fields = new List<Field>(
                RecordFields
                .ValidTypeFields
                .Select(node => new Field() { Name = node.FieldInfo.Name, Type = node.FieldInfo.Type })
                );
        }

        public override void GenerateDeclaration(CodeGenerator cg)
        {
            // do nothing...
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // do nothing...
        }
    }

}
