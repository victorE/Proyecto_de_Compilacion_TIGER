﻿using Antlr.Runtime;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration.Type
{
    public class DeclarationArrayNode : TypeDeclarationNode
    {
        #region Constructors

        public DeclarationArrayNode() : base() { }

        public DeclarationArrayNode(IToken token) : base(token) { }

        public DeclarationArrayNode(DeclarationArrayNode node) : base(node) { }

        #endregion

        public TypeReferenceNode ElementsType
        {
            get { return Children[1] as TypeReferenceNode; }
        }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Checking base...
            base.CheckDeclaration(scope, report);
            if (!IsOk)
                return;

            DeclaredType = new ArrayType(Name);
            scope.DefineType(DeclaredType);
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            ElementsType.CheckSemantics(scope, report);

            // Verifying declaration...
            if (!IsOk)
                return;

            // Checking children & declaration error...
            _IsOk = ElementsType.IsOk;
            (DeclaredType as ArrayType).ElementsType = ElementsType.ReferencedType;
        }

        public override void RectifyType(Scope scope, Report report)
        {
            // pass
        }

        public override void GenerateDeclaration(CodeGenerator cg)
        {
            // pass
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // pass
        }
    }

}
