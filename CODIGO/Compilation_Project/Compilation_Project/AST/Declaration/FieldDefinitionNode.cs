﻿using Antlr.Runtime;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration
{
    public class FieldDefinitionNode : LanguageNode
    {
        #region Constructors

        public FieldDefinitionNode() : base() { }

        public FieldDefinitionNode(IToken token) : base(token) { }

        public FieldDefinitionNode(FieldDefinitionNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public string FieldName
        {
            get { return (Children[0] as IdNode).Text; }
        }

        public TypeReferenceNode FieldType
        {
            get { return Children[1] as TypeReferenceNode; }
        }

        public VariableInfo FieldInfo { get; private set; }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking children...
            FieldType.CheckSemantics(scope, report);
            if (!FieldType.IsOk)
            {
                _IsOk = false;
                return;
            }

            // Ok
            _IsOk = true;
            FieldInfo = new VariableInfo()
            {
                IsParameter = true,
                Name = FieldName,
                Type = FieldType.ReferencedType
            };
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            // do nothing...
        }
    }

}
