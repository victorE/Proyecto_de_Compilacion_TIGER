﻿using Antlr.Runtime;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST.Declaration
{
    public abstract class TypeDeclarationNode : DeclarationNode
    {
        #region Constructors

        public TypeDeclarationNode() : base() { }

        public TypeDeclarationNode(IToken token) : base(token) { }

        public TypeDeclarationNode(TypeDeclarationNode node) : base(node) { }

        #endregion

        public string Name
        {
            get { return (Children[0] as IdNode).Text; }
        }

        public virtual CustomType DeclaredType { get; protected set; }

        public override void CheckDeclaration(Scope scope, Report report)
        {
            // Checking type existance...
            if (scope.ExistTypeInCurrentContext(Name))
            {
                _IsOk = false;
                report.Add(SemanticInfo.DuplicatedType(Name, this));
                return;
            }

            // Ok
            _IsOk = true;
        }

        public void NotifyError()
        {
            _IsOk = false;
        }

        public abstract void RectifyType(Scope scope, Report report);
    }
}
