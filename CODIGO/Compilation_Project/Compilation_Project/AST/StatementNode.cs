﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime;

namespace Compilation_Project.AST
{
    public abstract class StatementNode : ExpressionNode
    {

        public StatementNode() : base() { }

        public StatementNode(IToken token) : base(token) { }

        public StatementNode(StatementNode node) : base(node) { }

    }
}
