﻿using Antlr.Runtime;
using Compilation_Project.Basic_Types;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public abstract class UnaryExpressionNode : ExpressionNode
    {
        #region Constructors

        public UnaryExpressionNode() : base() { }

        public UnaryExpressionNode(IToken token) : base(token) { }

        public UnaryExpressionNode(UnaryExpressionNode node) : base(node) { }

        #endregion

        public ExpressionNode Operand
        {
            get { return Children[0] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking operand...
            Operand.CheckSemantics(scope, report);
            if (!Operand.IsOk)
                ExpressionType = TigerType.Error;
        }
    }

}
