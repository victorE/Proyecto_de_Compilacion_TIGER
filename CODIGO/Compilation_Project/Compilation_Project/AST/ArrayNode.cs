﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class ArrayNode : ExpressionNode
    {
        #region Constructors

        public ArrayNode() : base() { }

        public ArrayNode(IToken token) : base(token) { }

        public ArrayNode(ArrayNode node) : base(node) { }

        #endregion

        public TypeReferenceNode ArrayType
        {
            get { return Children[0] as TypeReferenceNode; }
        }

        public ExpressionNode Length
        {
            get { return Children[1] as ExpressionNode; }
        }

        public ExpressionNode InitialValues
        {
            get { return Children[2] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking childrens...
            ArrayType.CheckSemantics(scope, report);
            Length.CheckSemantics(scope, report);
            InitialValues.CheckSemantics(scope, report);
            if (!ArrayType.IsOk || !Length.IsOk || !InitialValues.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Type-checking type...
            ArrayType type = ArrayType.ReferencedType as ArrayType;
            if (type == null)
            {
                report.Add(SemanticInfo.NonArrayType(ArrayType.ReferencedType.Name, ArrayType));
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = ArrayType.ReferencedType;

            // Type-checking length...
            if (!TigerType.AreCompatibles(Length.ExpressionType, TigerType.Integer))
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.InvalidLength(Length.ExpressionType.Name, Length));
            }

            // Type-checking values...
            if (!TigerType.AreCompatibles(type.ElementsType, InitialValues.ExpressionType))
            {
                ExpressionType = TigerType.Error;
                report.Add(SemanticInfo.IncompatibleTypes(type.ElementsType.Name, InitialValues.ExpressionType.Name, InitialValues));
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            Length.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Dup);

            LocalBuilder length = cg.Generator.DeclareLocal(typeof(int));
            cg.Generator.Emit(OpCodes.Stloc, length);

            System.Type elementsType = (ArrayType.ReferencedType as ArrayType).ElementsType.GetCILType(cg.ModuleBuilder);
            cg.Generator.Emit(OpCodes.Newarr, elementsType);

            Label start = cg.Generator.DefineLabel();
            Label end = cg.Generator.DefineLabel();

            LocalBuilder index = cg.Generator.DeclareLocal(typeof(int));
            cg.Generator.Emit(OpCodes.Ldc_I4_0);
            cg.Generator.Emit(OpCodes.Stloc, index);

            // init loop
            cg.Generator.MarkLabel(start);

            // condition
            cg.Generator.Emit(OpCodes.Ldloc, index);
            cg.Generator.Emit(OpCodes.Ldloc, length);
            cg.Generator.Emit(OpCodes.Bge, end);

            // load index...
            cg.Generator.Emit(OpCodes.Dup);
            cg.Generator.Emit(OpCodes.Ldloc, index);
            InitialValues.GenerateCode(cg);
            cg.Generator.Emit(OpCodes.Stelem, elementsType);

            // continue
            cg.Generator.Emit(OpCodes.Ldloc, index);
            cg.Generator.Emit(OpCodes.Ldc_I4_1);
            cg.Generator.Emit(OpCodes.Add);
            cg.Generator.Emit(OpCodes.Stloc, index);

            cg.Generator.Emit(OpCodes.Br, start);

            // end loop
            cg.Generator.MarkLabel(end);
        }
    }

}
