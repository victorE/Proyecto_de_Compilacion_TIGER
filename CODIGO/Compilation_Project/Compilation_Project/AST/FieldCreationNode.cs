﻿using Antlr.Runtime;
using Compilation_Project.AST.Auxiliary;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class FieldCreationNode : LanguageNode
    {
        #region Constructors

        public FieldCreationNode() : base() { }

        public FieldCreationNode(IToken token) : base(token) { }

        public FieldCreationNode(FieldCreationNode node) : base(node) { }

        #endregion

        protected bool _IsOk;
        public override bool IsOk
        {
            get { return _IsOk; }
        }

        public string FieldName
        {
            get { return (Children[0] as IdNode).Name; }
        }

        public ExpressionNode Value
        {
            get { return Children[1] as ExpressionNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking children...
            Value.CheckSemantics(scope, report);

            // Ok
            _IsOk = Value.IsOk;
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            Value.GenerateCode(cg);
        }
    }

}
