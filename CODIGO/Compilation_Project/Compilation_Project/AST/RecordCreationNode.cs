﻿using Antlr.Runtime;
using System.Reflection.Emit;
using Compilation_Project.CodeGeneration;
using Compilation_Project.Basic_Types;
using Compilation_Project.Basic_Types.CustomTypes;
using Compilation_Project.Semantic;

namespace Compilation_Project.AST
{
    public class RecordCreationNode : ExpressionNode
    {
        #region Constructors

        public RecordCreationNode() : base() { }

        public RecordCreationNode(IToken token) : base(token) { }

        public RecordCreationNode(RecordCreationNode node) : base(node) { }

        #endregion

        public FieldsCreationNode ValueFields
        {
            get { return Children[1] as FieldsCreationNode; }
        }

        public TypeReferenceNode RecordType
        {
            get { return Children[0] as TypeReferenceNode; }
        }

        public override void CheckSemantics(Scope scope, Report report)
        {
            // Checking childrens...
            ValueFields.CheckSemantics(scope, report);
            RecordType.CheckSemantics(scope, report);
            if (!ValueFields.IsOk || !RecordType.IsOk)
            {
                ExpressionType = TigerType.Error;
                return;
            }

            // Type-checking type...
            RecordType type = RecordType.ReferencedType as RecordType;
            if (type == null)
            {
                report.Add(SemanticInfo.NonRecordType(RecordType.ReferencedType.Name, RecordType));
                ExpressionType = TigerType.Error;
                return;
            }

            // Checking number of fields...
            if (type.Fields.Count != ValueFields.ChildCount)
            {
                report.Add(SemanticInfo.WrongFieldsNumber(type.Name, type.Fields.Count, ValueFields.ChildCount, this));
                ExpressionType = TigerType.Error;
                return;
            }

            // Ok
            ExpressionType = type;

            // Type-checking fields & names...
            int i = 0;
            foreach (var value in ValueFields.Fields)
            {
                var expectedField = type.Fields[i++];
                var actualField = new Field()
                {
                    Name = value.FieldName,
                    Type = value.Value.ExpressionType
                };

                if (expectedField.Name != actualField.Name)
                {
                    report.Add(SemanticInfo.WrongFieldPosition(expectedField.Name, actualField.Name, value));
                    ExpressionType = TigerType.Error;
                }

                if (!TigerType.AreCompatibles(expectedField.Type, actualField.Type))
                {
                    report.Add(SemanticInfo.IncompatibleTypes(expectedField.Type.Name, actualField.Type.Name, value));
                    ExpressionType = TigerType.Error;
                }
            }
        }

        public override void GenerateCode(CodeGenerator cg)
        {
            RecordType record = RecordType.ReferencedType as RecordType;
            cg.Generator.Emit(OpCodes.Newobj, record.GetCILType(cg.ModuleBuilder).GetConstructor(System.Type.EmptyTypes));

            int i = 0;
            foreach (var field in ValueFields.Fields)
            {
                cg.Generator.Emit(OpCodes.Dup);
                field.GenerateCode(cg);
                cg.Generator.Emit(OpCodes.Stfld, record.Fields[i++].GeneratedField);
            }
        }
    }

}
